package telkom.com.app_anggota.Adapter;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import telkom.com.app_anggota.Data.Piutang;
import telkom.com.app_anggota.Dialog.PiutDetFragment;
import telkom.com.app_anggota.R;

/**
 * Created by sukri on 01/12/16.
 */

public class AdapterPiutang extends BaseAdapter {
    private FragmentManager fargment;
    private TextView tv1, tv2;
    private TextView tv3, tv4;
    private TextView tv5, tv6;
    private TextView tv7, tv8;
    private TextView tv9, tv10;
    private TextView tv11, tv12;
    private TextView tv13;
    private Button btnDetail;

    private LayoutInflater layoutInflater;
    private ArrayList<Piutang> piutangs;
    private TextView tv14;
    private Context context;

    public AdapterPiutang(Context context, ArrayList<Piutang> piutangs, FragmentManager fm) {
        layoutInflater = LayoutInflater.from(context);
        this.piutangs = piutangs;
        this.fargment = fm;
        this.context = context;
    }

    public AdapterPiutang(Context context, ArrayList<Piutang> piutangs) {
        layoutInflater = LayoutInflater.from(context);
        this.piutangs = piutangs;
        this.context = context;
    }

    @Override
    public int getCount() {
        return piutangs.size();
    }

    @Override
    public Object getItem(int position) {
        return piutangs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_piutang, null);
        }
        tv1 = (TextView) convertView.findViewById(R.id.nopinj);
        tv2 = (TextView) convertView.findViewById(R.id.keterangan);
        tv3 = (TextView) convertView.findViewById(R.id.plafond);
        tv4 = (TextView) convertView.findViewById(R.id.outstanding);
        tv5 = (TextView) convertView.findViewById(R.id.tglakad);
        tv6 = (TextView) convertView.findViewById(R.id.tglangs);
        tv7 = (TextView) convertView.findViewById(R.id.jangkawaktu);
        tv8 = (TextView) convertView.findViewById(R.id.jatuhtempo);
        tv9 = (TextView) convertView.findViewById(R.id.sisaangs);
        tv14 = (TextView) convertView.findViewById(R.id.ke);
        tv10 = (TextView) convertView.findViewById(R.id.call);
        tv11 = (TextView) convertView.findViewById(R.id.pokok);
        tv12 = (TextView) convertView.findViewById(R.id.bunga);
        tv13 = (TextView) convertView.findViewById(R.id.total);
        btnDetail = (Button) convertView.findViewById(R.id.button);

        final Piutang a = piutangs.get(position);

        tv1.setText(a.getNOPINJ());
        tv2.setText(a.getKETERANGAN());
        tv3.setText(a.getPLAFOND());
        tv4.setText(a.getOUTSTANDING());
        tv5.setText(a.getTGLAKAD());
        tv6.setText(a.getTGLANGS());
        btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PiutDetFragment piutDetFragment = new PiutDetFragment().newInstance(a.getMutasi());
                piutDetFragment.show(fargment, "Detail");
            }
        });
        tv7.setText(a.getJW());
        tv8.setText(a.getJATUHTEMPO());
        tv9.setText(a.getSBP());
        tv14.setText(a.getKE());
        tv10.setText(a.getCALL());
        tv11.setText(a.getANGSPOKOK());
        tv12.setText(a.getANGSPOKOK());
        tv13.setText(a.getANGSURAN());


        return convertView;
    }
}
