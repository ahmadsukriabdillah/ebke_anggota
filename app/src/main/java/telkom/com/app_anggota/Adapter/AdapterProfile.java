package telkom.com.app_anggota.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import telkom.com.app_anggota.Data.Profile;
import telkom.com.app_anggota.R;

/**
 * Created by sukri on 29/11/16.
 */

public class AdapterProfile extends BaseAdapter {
    private TextView tv1, tv2;
    private ImageView imageView;
    private LayoutInflater layoutInflater;
    private ArrayList<Profile> data;

    public AdapterProfile(Context context, ArrayList<Profile> data) {
        this.data = data;
        this.layoutInflater = layoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_profile, null);
        }

        Profile a = data.get(position);
        tv1 = (TextView) convertView.findViewById(R.id.title);
        tv2 = (TextView) convertView.findViewById(R.id.ket);
        //imageView = (ImageView) convertView.findViewById(R.id.img);

        tv1.setText(a.getTitle());
        tv2.setText(a.getKeterangan());
        //imageView.setImageResource(a.getImage());

        return convertView;
    }
}
