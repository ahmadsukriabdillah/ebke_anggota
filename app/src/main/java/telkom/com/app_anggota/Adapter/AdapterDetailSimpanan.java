package telkom.com.app_anggota.Adapter;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import telkom.com.app_anggota.Data.ListSimpanan;
import telkom.com.app_anggota.R;

/**
 * Created by sukri on 01/12/16.
 */
public class AdapterDetailSimpanan extends BaseAdapter {
    private FragmentManager fm;
    private TextView tv1, tv2, tv3, tv4, tv5, tv6;
    private LayoutInflater layoutInflater;
    private ArrayList<ListSimpanan> data;

    public AdapterDetailSimpanan(Context context, ArrayList<ListSimpanan> data) {
        this.data = data;
        this.layoutInflater = layoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_detail_simpanan, null);
        }
        tv1 = (TextView) convertView.findViewById(R.id.tgl);
        tv2 = (TextView) convertView.findViewById(R.id.keterangan);
        tv3 = (TextView) convertView.findViewById(R.id.berita);
        tv4 = (TextView) convertView.findViewById(R.id.debet);
        tv5 = (TextView) convertView.findViewById(R.id.kredit);
        tv6 = (TextView) convertView.findViewById(R.id.saldo);


        ListSimpanan a = data.get(position);
        tv1.setText(a.getTglStor());
        tv2.setText(a.getKeterangan());
        tv3.setText(a.getBerita());
        tv4.setText(a.getDebit());
        tv5.setText(a.getKredit());
        tv6.setText(a.getSaldo());
        return convertView;
    }
}
