package telkom.com.app_anggota.Adapter;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import telkom.com.app_anggota.Data.Deposito;
import telkom.com.app_anggota.Dialog.DepDetFragment;
import telkom.com.app_anggota.R;

/**
 * Created by sukri on 25/11/16.
 */

public class AdapterDeposito extends BaseAdapter {
    private FragmentManager fm;
    private TextView tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9;
    private Button btn;
    private LayoutInflater layoutInflater;
    private ArrayList<Deposito> data;

    public AdapterDeposito(Context context, ArrayList<Deposito> data, FragmentManager fm) {
        this.data = data;
        this.fm = fm;
        this.layoutInflater = layoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_deposito, null);
        }
        btn = (Button) convertView.findViewById(R.id.button);
        tv1 = (TextView) convertView.findViewById(R.id.nodep);
        tv2 = (TextView) convertView.findViewById(R.id.keterangan);
        tv3 = (TextView) convertView.findViewById(R.id.tglbuka);
        tv4 = (TextView) convertView.findViewById(R.id.tglakad);
        tv5 = (TextView) convertView.findViewById(R.id.tglcair);
        tv6 = (TextView) convertView.findViewById(R.id.jw);
        tv7 = (TextView) convertView.findViewById(R.id.pajak);
        tv8 = (TextView) convertView.findViewById(R.id.bunga);
        tv9 = (TextView) convertView.findViewById(R.id.nominal);
        final Deposito a = data.get(position);
        tv1.setText(a.getNODEP());
        tv2.setText(a.getKETERANGAN());
        tv3.setText(a.getTGLBUKA());
        tv4.setText(a.getTGLAKTIF());
        tv5.setText(a.getTGLCAIR());
        tv6.setText(a.getJW());
        tv7.setText(a.getPAJAK());
        tv8.setText(a.getBUNGA());
        tv9.setText(a.getNOMINAL());

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DepDetFragment piutDetFragment = new DepDetFragment().newInstance(a.getMutasi());
                piutDetFragment.show(fm, "Detail");
            }
        });
        return convertView;
    }
}
