package telkom.com.app_anggota.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import telkom.com.app_anggota.Data.DetailPiutang;
import telkom.com.app_anggota.R;

/**
 * Created by sukri on 01/12/16.
 */
public class AdapterDetailPiutang extends BaseAdapter {
    private TextView tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10;
    private LayoutInflater layoutInflater;
    private ArrayList<DetailPiutang> data;

    public AdapterDetailPiutang(Context context, ArrayList<DetailPiutang> data) {
        this.data = data;
        this.layoutInflater = layoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_detail_piutang, null);
        }
        tv1 = (TextView) convertView.findViewById(R.id.tanggal);
        tv2 = (TextView) convertView.findViewById(R.id.keterangan);
        tv3 = (TextView) convertView.findViewById(R.id.ke);
        tv4 = (TextView) convertView.findViewById(R.id.call);
        tv5 = (TextView) convertView.findViewById(R.id.pokok);
        tv6 = (TextView) convertView.findViewById(R.id.sisa);
        tv7 = (TextView) convertView.findViewById(R.id.denda);
        tv8 = (TextView) convertView.findViewById(R.id.pinalty);
        tv9 = (TextView) convertView.findViewById(R.id.angsuran);
        tv10 = (TextView) convertView.findViewById(R.id.outstanding);

        DetailPiutang a = data.get(position);
        tv1.setText(a.getTANGGAL());
        tv2.setText(a.getKETERANGAN());
        tv3.setText(a.getKE());
        tv4.setText(a.getCOLL());
        tv5.setText(a.getANGSPOKOK());
        tv6.setText(a.getSBP());
        tv7.setText(a.getDENDA());
        tv8.setText(a.getPENALTY());
        tv9.setText(a.getANGSURAN());
        tv10.setText(a.getOUTSTANDING());
        return convertView;
    }
}
