package telkom.com.app_anggota.Adapter;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import telkom.com.app_anggota.Data.Simpanan;
import telkom.com.app_anggota.Dialog.SimpDetFragment;
import telkom.com.app_anggota.R;

/**
 * Created by sukri on 25/11/16.
 */

public class AdapterSimpanan extends BaseAdapter {
    private FragmentManager fm;
    private TextView tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8;
    private Button btn;
    private LayoutInflater layoutInflater;
    private ArrayList<Simpanan> data;

    public AdapterSimpanan(Context context, ArrayList<Simpanan> data, FragmentManager fm) {
        this.data = data;
        this.fm = fm;
        this.layoutInflater = layoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_simpanan, null);
        }
        btn = (Button) convertView.findViewById(R.id.button);
        tv1 = (TextView) convertView.findViewById(R.id.nosimp);
        tv2 = (TextView) convertView.findViewById(R.id.keterangan);
        tv3 = (TextView) convertView.findViewById(R.id.tglbuka);
        tv4 = (TextView) convertView.findViewById(R.id.tglaktif);
        tv5 = (TextView) convertView.findViewById(R.id.tglbeban);
        tv6 = (TextView) convertView.findViewById(R.id.debet);
        tv7 = (TextView) convertView.findViewById(R.id.kredit);
        tv8 = (TextView) convertView.findViewById(R.id.saldo);
        final Simpanan a = data.get(position);
        tv1.setText(a.getNosimp());
        tv2.setText(a.getKeterangan());
        tv3.setText(a.getTglbuka());
        tv4.setText(a.getTglaktif());
        tv5.setText(a.getTglbeban());
        tv6.setText(a.getDebet());
        tv7.setText(a.getKredit());
        tv8.setText(a.getSaldo());

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimpDetFragment piutDetFragment = new SimpDetFragment().newInstance(a.getSimp());
                piutDetFragment.show(fm, "Detail");
            }
        });
        return convertView;
    }
}
