package telkom.com.app_anggota;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Created by sukri on 08/11/16.
 */

public class AdapterBottomBar extends FragmentPagerAdapter {

    private ArrayList<CustomFragment> fragments = new ArrayList<>();
    private CustomFragment currentFragment;

    public AdapterBottomBar(FragmentManager fm) {
        super(fm);

        fragments.clear();
        fragments.add(CustomFragment.newInstance(0));
        fragments.add(CustomFragment.newInstance(1));
        fragments.add(CustomFragment.newInstance(2));
        fragments.add(CustomFragment.newInstance(3));
    }

    @Override
    public CustomFragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        if (getCurrentFragment() != object) {
            currentFragment = ((CustomFragment) object);
        }
        super.setPrimaryItem(container, position, object);
    }

    /**
     * Get the current fragment
     */
    public CustomFragment getCurrentFragment() {
        return currentFragment;
    }
}
