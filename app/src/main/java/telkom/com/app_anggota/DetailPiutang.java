package telkom.com.app_anggota;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import telkom.com.app_anggota.Adapter.AdapterDetailPiutang;
import telkom.com.app_anggota.Helper.AppConfig;
import telkom.com.app_anggota.Helper.AppController;
import telkom.com.app_anggota.Helper.SessionManager;

public class DetailPiutang extends AppCompatActivity {
    private SessionManager session;
    private ListView listView;
    private ArrayList<telkom.com.app_anggota.Data.DetailPiutang> detailPiutangs;
    private AdapterDetailPiutang adapterDetailPiutang;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_piutang);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        session = new SessionManager(this);
        listView = (ListView) findViewById(R.id.list);
        detailPiutangs = new ArrayList<>();
        adapterDetailPiutang = new AdapterDetailPiutang(getApplicationContext(), detailPiutangs);
        listView.setAdapter(adapterDetailPiutang);
        fetchDetail(getIntent().getStringExtra("NOPINJ"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
    }

    private void fetchDetail(String nopiut) {
        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.DETAIL_PIUTANG + "?cif=" + session.getCIF() + "&NOPINJ=" + nopiut, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    detailPiutangs.clear();
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getBoolean("status")) {
                        JSONArray simp = jsonObject.getJSONArray("data");
                        for (int i = 0; i < simp.length(); i++) {
                            JSONObject obj = simp.getJSONObject(i);
                            telkom.com.app_anggota.Data.DetailPiutang data = new telkom.com.app_anggota.Data.DetailPiutang();
                            data.setUID(obj.getString("UID"));
                            data.setTANGGAL(obj.getString("TANGGAL"));
                            data.setANGSBUNGA(obj.getString("ANGSBUNGA"));
                            data.setANGSBUNGA_CONVR(obj.getString("ANGSBUNGA"));
                            data.setANGSPOKOK(obj.getString("ANGSPOKOK"));
                            data.setANGSPOKOK_CONVR(obj.getString("ANGSPOKOK_CONVR"));
                            data.setBLN(obj.getString("BLN"));
                            data.setBERITA(obj.getString("BERITA"));
                            data.setDENDA(obj.getString("DENDA"));
                            data.setHARI(obj.getString("HARI"));
                            data.setCIF(obj.getString("CIF"));
                            data.setCOLL(obj.getString("COLL"));
                            data.setIDE(obj.getString("IDE"));
                            data.setJBL(obj.getString("JBL"));
                            data.setKE(obj.getString("KE"));
                            data.setOUTSTANDING(obj.getString("OUTSTANDING"));
                            data.setPLAFOND(obj.getString("PLAFOND"));
                            data.setKETERANGAN(obj.getString("KETERANGAN"));
                            data.setKU(obj.getString("KU"));
                            data.setNOMANGSBUNGA(obj.getString("NOMANGSBUNGA"));
                            data.setNOMANGSBUNGA_CONVR(obj.getString("NOMANGSBUNGA_CONVR"));
                            data.setNOMANGSPOKOK(obj.getString("NOMANGSPOKOK"));
                            data.setNOMPENALTY(obj.getString("NOMPENALTY"));
                            data.setSBP(obj.getString("SBP"));
                            data.setNOPINJ(obj.getString("NOPINJ"));
                            data.setTGLBEBAN(obj.getString("TGLBEBAN"));
                            detailPiutangs.add(data);
                        }
                        adapterDetailPiutang.notifyDataSetChanged();


                    } else {

                    }


                } catch (JSONException e) {

                    Log.e("login", "Login Error: " + e.getMessage());
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
//               Log.e("login", "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        "Terjadi Kesalahan", Toast.LENGTH_LONG).show();

            }
        });

        AppController.getInstance().addToRequestQueue(strReq, "get Piutang");

    }


}
