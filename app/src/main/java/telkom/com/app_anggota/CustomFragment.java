package telkom.com.app_anggota;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import telkom.com.app_anggota.Adapter.AdapterProfile;
import telkom.com.app_anggota.AkunSaya.AkunSaya;
import telkom.com.app_anggota.Data.Profile;
import telkom.com.app_anggota.Helper.AppConfig;
import telkom.com.app_anggota.Helper.AppController;
import telkom.com.app_anggota.Helper.CallDialog;
import telkom.com.app_anggota.Helper.InputValidator;
import telkom.com.app_anggota.Helper.SessionManager;

/**
 * Created by sukri on 08/11/16.
 */

public class CustomFragment extends Fragment implements ViewPagerEx.OnPageChangeListener, BaseSliderView.OnSliderClickListener {

    private FrameLayout fragmentContainer;
    private SliderLayout slideView;
    private ArrayList<String> slideimage;
    private Button btnAkunSaya, btnSimulasihitung;

    /**
     * init keluar
     */
    private Button btnYes, btnNo;

    /**
     * init profile
     */
    private SessionManager session;
    private ListView listView;
    private ArrayList<Profile> data;
    private AdapterProfile adapterProfile;


    /**
     * INITIAL VIEW AND DATA CHPASS
     */


    private EditText old, pass, cpass;
    private Button btnch;
    private ProgressDialog pDialog;
    private int tray = 0;

    /**
     * Create a new instance of the fragment
     */
    public static CustomFragment newInstance(int index) {
        CustomFragment fragment = new CustomFragment();
        Bundle b = new Bundle();
        b.putInt("index", index);
        fragment.setArguments(b);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getArguments().getInt("index", 0) == 0) {
            View view = inflater.inflate(R.layout.fragment_home, container, false);
            initHome(view);
            return view;
        } else if (getArguments().getInt("index", 0) == 3) {
            View view = inflater.inflate(R.layout.fragment_keluar, container, false);
            initKeluar(view);
            return view;
        } else if (getArguments().getInt("index", 0) == 2) {
            View view = inflater.inflate(R.layout.fragment_password, container, false);
            initPass(view);
            return view;
        } else {
            View view = inflater.inflate(R.layout.fragment_profile, container, false);
            initProfile(view);
            return view;
        }
    }

    private void initPass(View v) {
        old = (EditText) v.findViewById(R.id.oldpass);
        pass = (EditText) v.findViewById(R.id.newpass);
        cpass = (EditText) v.findViewById(R.id.cnewpass);
        btnch = (Button) v.findViewById(R.id.btnch);
    }

    private void initProfile(View view) {
        listView = (ListView) view.findViewById(R.id.list);
    }

    private void initKeluar(View view) {
        btnNo = (Button) view.findViewById(R.id.btnNo);
        btnYes = (Button) view.findViewById(R.id.btnYes);


    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        switch (getArguments().getInt("index", 0)) {
            case 0:
                slideimage = new ArrayList<>();
                getSlide();
                slideView.setPresetTransformer(SliderLayout.Transformer.Default);


                slideView.setCustomAnimation(new DescriptionAnimation());
                slideView.setCustomIndicator((PagerIndicator) getActivity().findViewById(R.id.custom_indicator));
                slideView.setDuration(4000);

                slideView.addOnPageChangeListener(this);

                btnAkunSaya.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(getActivity(), AkunSaya.class));
                    }
                });

                btnSimulasihitung.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(getActivity(), SimulasiPerhitunganPinjaman.class));
                    }
                });
                break;
            case 1:
                data = new ArrayList<>();
                session = new SessionManager(getActivity());
                adapterProfile = new AdapterProfile(getActivity(), data);
                View header = getActivity().getLayoutInflater().inflate(R.layout.header_list, null);
                listView.addHeaderView(header);
                listView.setAdapter(adapterProfile);
                fetchProfile();
                break;
            case 2:
                pDialog = new ProgressDialog(getActivity());
                session = new SessionManager(getActivity());
                btnch.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        InputValidator iv = new InputValidator();
                        if (iv.isNullOrEmpty(old.getText().toString())) {
                            old.setError("Isi Terlebih dahulu");
                            old.requestFocus();
                            return;
                        }
                        if (iv.isNullOrEmpty(pass.getText().toString())) {
                            old.setError("Isi Terlebih dahulu");
                            old.requestFocus();
                            return;
                        }
                        if (iv.isNullOrEmpty(cpass.getText().toString())) {
                            old.setError("Isi Terlebih dahulu");
                            old.requestFocus();
                            return;
                        }

                        changepass(old.getText().toString().trim(), cpass.getText().toString().trim());

                    }
                });


                break;
            case 3:
                btnYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getActivity().finish();
                    }

                });

                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
                break;
        }
    }

    private void changepass(final String oldpass, final String newpass) {
        String tag_string_req = "reg chpass";

        pDialog.setMessage("Loading..");
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.CHPASS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                //Log.d("login", "Login Response: " + response.toString());
                pDialog.hide();

                try {

                    JSONObject object = new JSONObject(response);
                    if (object.getBoolean("status")) {
                        old.setText("");
                        pass.setText("");
                        cpass.setText("");
                        Toast.makeText(getActivity(), object.getString("msg"), Toast.LENGTH_LONG).show();
                    } else {
                        old.setError(object.getString("msg"));
                        old.requestFocus();
                        old.setText("");
                        pass.setText("");
                        cpass.setText("");
                        Toast.makeText(getActivity(), object.getString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e("login", "Login Error: " + error.getMessage());
//                Toast.makeText(getActivity(),
//                        error.getMessage(), Toast.LENGTH_LONG).show();
                pDialog.hide();
                if (tray < 5) {
                    tray++;
                    CallDialog.showMassage(getActivity(), "Erorr", "Terjadi Kesalahan Coba Lagi");
                } else {
                    CallDialog.showMassage(getActivity(), "Erorr", "Server Bermasalah coba Beberapa saat kemudian");
                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("NEWPASS", newpass);
                params.put("OLDPASS", oldpass);
                params.put("UID", session.getUSERNAME());

                return params;
            }

        };
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        // Adding request to request queue
    }


    @Override
    public void onStop() {
        if (getArguments().getInt("index") == 0) {
            slideView.stopAutoCycle();
        }
        super.onStop();
    }

    @Override
    public void onResume() {
        if (getArguments().getInt("index") == 0) {
            slideView.startAutoCycle();
        }
        super.onResume();
    }

    private void getSlide() {

        String tag_string_req = "req_login";

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.SLIDE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                Log.d("login", "Login Response: " + response.toString());

                try {
                    slideimage.clear();
                    JSONArray jObj = new JSONArray(response);
                    for (int i = 0; i < jObj.length(); i++) {
                        JSONObject obj = jObj.getJSONObject(i);
                        slideimage.add(obj.getString("NAMA_FILE"));
                    }
                    start();

                } catch (JSONException e) {
                    // JSON error
//                    e.printStackTrace();
//                    Toast.makeText(getActivity(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("login", "Login Error: " + error.getMessage());
//                Toast.makeText(getActivity(),
//                        "Terjadi Kesalahan", Toast.LENGTH_LONG).show();
                Handler a = new Handler();
                a.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getSlide();
                    }
                }, 3000);

            }
        });

        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);


    }


    private void fetchProfile() {
        String tag_string_req = "req_login";

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.PROFILE + "?cif=" + session.getCIF() + "&cib=" + session.getCIB(), new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject cjObj = new JSONObject(response);
                    if (cjObj.getBoolean("status")) {
                        data.clear();
                        JSONObject jObj = cjObj.getJSONObject("data");
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "NO CIB", jObj.getString("CIB")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "NO KARTU", jObj.getString("NOKARTU")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "NIP", jObj.getString("NIP")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "NAMA", jObj.getString("NAMA")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "KODE UNIT", jObj.getString("KI")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "PANGGILAN", jObj.getString("PANGGILAN")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "KELAMIN", jObj.getString("KL")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "TEMPAT LAHIR", jObj.getString("TMP_LAHIR")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "TANGGAL LAHIR", jObj.getString("TGL_LAHIR")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "TIPE ID", jObj.getString("ID")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "NO ID", jObj.getString("NO_ID")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "TANGGAL EXPIRED", jObj.getString("TGLEXPIRED")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "AGAMA", jObj.getString("AGAMA")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "PENDIDIKAN", jObj.getString("PENDIDIKAN")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "PEKERJAAN", jObj.getString("PEKERJAAN")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "JABATAN", jObj.getString("JABATAN")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "GOLONGAN", jObj.getString("GOLONGAN")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "ALAMAT", jObj.getString("ALAMAT")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "TELEPON", jObj.getString("TELEPON")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "HANDPHONE", jObj.getString("HANDPHONE")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "E MAIL", jObj.getString("E_MAIL")));
                    } else {
                        Toast.makeText(getActivity(), "Gagal Memuat data", Toast.LENGTH_SHORT).show();
                    }
                    adapterProfile.notifyDataSetChanged();
                } catch (JSONException e) {
//                    e.printStackTrace();
//                    Toast.makeText(getActivity(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e("login", "Login Error: " + error.getMessage());
//                Toast.makeText(getActivity(),
//                        "Terjadi kesalahan", Toast.LENGTH_LONG).show();
                if (tray < 5) {
                    tray++;
                    CallDialog.showMassage(getActivity(), "Erorr", "Terjadi Kesalahan Coba Lagi");
                } else {
                    CallDialog.showMassage(getActivity(), "Erorr", "Server Bermasalah coba Beberapa saat kemudian");
                }

            }
        });

        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    private void start() {
        for (int i = 0; i < slideimage.size(); i++) {
            DefaultSliderView textSliderView = new DefaultSliderView(getActivity());
            textSliderView.image(slideimage.get(i)).setScaleType(BaseSliderView.ScaleType.Fit).setOnSliderClickListener(this);
            slideView.addSlider(textSliderView);
        }

    }

    private void initHome(View view) {
        slideView = (SliderLayout) view.findViewById(R.id.slider);
        btnAkunSaya = (Button) view.findViewById(R.id.akunsaya);
        btnSimulasihitung = (Button) view.findViewById(R.id.simulasi);

    }

    public void willBeDisplayed() {
        // Do what you want here, for example animate the content
        if (fragmentContainer != null) {
            Animation fadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
            fragmentContainer.startAnimation(fadeIn);
        }
    }

    /**
     * Called when a fragment will be hidden
     */
    public void willBeHidden() {
        if (fragmentContainer != null) {
            Animation fadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
            fragmentContainer.startAnimation(fadeOut);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }
}