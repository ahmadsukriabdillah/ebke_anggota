package telkom.com.app_anggota.Dialog;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

import telkom.com.app_anggota.Adapter.AdapterDetailDeposito;
import telkom.com.app_anggota.Data.DetailDeposito;
import telkom.com.app_anggota.R;

/**
 * Created by sukri on 22/11/16.
 */

public class DepDetFragment extends DialogFragment {
    private Button btn;
    private ListView listView;
    private AdapterDetailDeposito adapterDetailDeposito;
    private ArrayList<DetailDeposito> detailDepositos;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onResume() {
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((WindowManager.LayoutParams) params);
        super.onResume();
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FragmentManager fm = getFragmentManager();
        adapterDetailDeposito = new AdapterDetailDeposito(getActivity(), detailDepositos);
        listView.setAdapter(adapterDetailDeposito);
        adapterDetailDeposito.notifyDataSetChanged();
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public DepDetFragment newInstance(ArrayList<DetailDeposito> pojoPiutang) {
        DepDetFragment a = new DepDetFragment();
        a.setData(pojoPiutang);
        a.setStyle(DialogFragment.STYLE_NORMAL, 0);
        return a;
    }

    public void setData(ArrayList<DetailDeposito> pojoPiutang) {
        this.detailDepositos = pojoPiutang;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.detail_piutang_fragment, null);
        listView = (ListView) rootView.findViewById(R.id.list);
        btn = (Button) rootView.findViewById(R.id.tutup);
        getDialog().setTitle("Detail");
        return rootView;
    }
}
