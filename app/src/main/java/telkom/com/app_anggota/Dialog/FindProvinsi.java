package telkom.com.app_anggota.Dialog;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import telkom.com.app_anggota.Data.listData;
import telkom.com.app_anggota.Helper.AppConfig;
import telkom.com.app_anggota.Helper.AppController;
import telkom.com.app_anggota.R;

/**
 * Created by sukri on 22/11/16.
 */

public class FindProvinsi extends DialogFragment {
    Button btn;
    ListView lv;
    SearchView sv;
    ArrayAdapter<listData> adapter;
    PilihDialog pilihDialog;
    ArrayList<listData> list;
    private TextView empty;
    private ProgressBar pbar;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        list = new ArrayList<>();
        list.clear();
        adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, list);
        lv.setAdapter(adapter);
        fetchData(getArguments().getInt("id"));
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                PilihDialog listener = (PilihDialog) getTargetFragment();
                listener.sendData(getArguments().getInt("id"), adapter.getItem(position));
                dismiss();
            }
        });
        sv.setQueryHint("Search..");
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String txt) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String txt) {
                adapter.getFilter().filter(txt);
                return false;
            }
        });
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dismiss();
            }
        });

    }

    private void fetchData(int id) {

        String tag_string_req = "req_login";
        String endpoint = AppConfig.LIST_DAFTAR;
        switch (id) {
            case 1:
                endpoint += "?id=1";
                break;
            case 2:
                endpoint += "?id=2&id_prop=" + getArguments().getString("title");
                break;
            case 3:
                endpoint += "?id=3&id_kab=" + getArguments().getString("title");
                break;
            case 4:
                endpoint += "?id=4&id_kec=" + getArguments().getString("title");
                break;
            case 5:
                endpoint += "?id=5";
                break;
            case 6:
                String a = getArguments().getString("title");
                String[] z = a.split(" ");
                endpoint += "?id=6&id_kel=" + z[0] + "&id_cab=" + z[1];
                break;
            case 7:
                endpoint += "?id=7&cif=" + getArguments().getString("title");
                break;
        }
        StringRequest strReq = new StringRequest(Request.Method.GET,
                endpoint, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("login", "Login Response: " + response.toString());

                try {
                    pbar.setVisibility(View.GONE);
                    JSONArray jObj = new JSONArray(response);
                    for (int i = 0; i < jObj.length(); i++) {
                        JSONObject obj = jObj.getJSONObject(i);
                        list.add(new listData(obj.getString("ID"), obj.getString("TEXT")));
                    }
                    adapter.notifyDataSetChanged();
                    if (jObj.length() == 0) {
                        empty.setVisibility(View.VISIBLE);


                    } else {
                        lv.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("login", "Login Error: " + error.getMessage());
                Toast.makeText(getActivity(),
                        error.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    public FindProvinsi newInstance(int id, String title) {
        FindProvinsi a = new FindProvinsi();
        Bundle b = new Bundle();
        b.putInt("id", id);
        b.putString("title", title);
        ;
        a.setArguments(b);
        a.setStyle(DialogFragment.STYLE_NORMAL, 0);
        return a;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.find, null);
        getDialog().setTitle("Cari..");
        lv = (ListView) rootView.findViewById(R.id.listView1);
        sv = (SearchView) rootView.findViewById(R.id.searchView1);
        btn = (Button) rootView.findViewById(R.id.dismiss);
        pbar = (ProgressBar) rootView.findViewById(R.id.ps);
        empty = (TextView) rootView.findViewById(R.id.nodata);
        //getDialog().getWindow().setSoftInputMode(
        //WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return rootView;
    }

    public interface PilihDialog {
        public void sendData(int id, listData data);
    }


}
