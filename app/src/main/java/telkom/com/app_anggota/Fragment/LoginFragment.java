package telkom.com.app_anggota.Fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import telkom.com.app_anggota.Helper.InputValidator;
import telkom.com.app_anggota.R;

/**
 * Created by sukri on 21/11/16.
 */

public class LoginFragment extends Fragment {
    private TextView daftar;
    private EditText username, password;
    private Button btnLogin;
    private ImpLogin impLogin;

    public LoginFragment() {
    }

    public LoginFragment newInstance() {
        LoginFragment loginFragment = new LoginFragment();
        return loginFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        View v = inflater.inflate(R.layout.login, container, false);
        daftar = (TextView) v.findViewById(R.id.daftat);
        username = (EditText) v.findViewById(R.id.username);
        password = (EditText) v.findViewById(R.id.password);
        btnLogin = (Button) v.findViewById(R.id.btnLogin);
        return v;
    }

    public void setErrorLogin() {
        password.setText("");
        password.setError("Username / Password salah");
        password.requestFocus();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity != null) {
            try {
                impLogin = (ImpLogin) activity;
            } catch (ClassCastException e) {
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
//        outState.putString("username",username.getText().toString().trim());
//        outState.putString("password",password.getText().toString().trim());
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //impLogin = (ImpLogin) getActivity();

//        if(savedInstanceState != null){
//            username.setText(savedInstanceState.getString("username"));
//            password.setText(savedInstanceState.getString("password"));
//        }
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkData();
            }
        });
        daftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                impLogin.toRegister();
            }
        });
    }

    private void checkData() {
        InputValidator iv = new InputValidator();
        if (iv.isNullOrEmpty(username.getText().toString())) {
            username.setError("Username perlu anda isi terlebih dahulu");
            username.requestFocus();
            return;
        } else if (iv.isNullOrEmpty(password.getText().toString())) {
            password.setError("Password perlu anda isi terlebih dahulu");
            password.requestFocus();
            return;
        } else {
            impLogin.login(username.getText().toString().trim(), password.getText().toString().trim());
        }
    }


}
