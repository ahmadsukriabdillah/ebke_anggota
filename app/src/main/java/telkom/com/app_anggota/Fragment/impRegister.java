package telkom.com.app_anggota.Fragment;

import java.util.HashMap;

/**
 * Created by sukri on 21/11/16.
 */

public interface impRegister {
    void register(HashMap<String, String> data);

    void toLogin();

    void openDialog(int id, String title);
}
