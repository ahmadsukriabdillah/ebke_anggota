package telkom.com.app_anggota.Fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.HashMap;

import telkom.com.app_anggota.Data.listData;
import telkom.com.app_anggota.Dialog.FindProvinsi;
import telkom.com.app_anggota.R;

/**
 * Created by sukri on 21/11/16.
 */

public class RegisterFragment extends Fragment implements FindProvinsi.PilihDialog {
    private static String prov, kab, kec, kel, cab, cif, cib;
    private impRegister impRegister;
    private EditText edtNokt, edtNama, edtAlamat, edtEmail, edtUserid, edtPass, edtCPass;
    private Button edtProv, edtKab, edtKec, edtKel, edtCab, edtMitra, edtCib, btnRegister;
    private ArrayList<listData> list;

    public RegisterFragment() {
    }

    public RegisterFragment newInstance() {
        RegisterFragment registerFragment = new RegisterFragment();
        return registerFragment;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        impRegister = (impRegister) activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        View v = inflater.inflate(R.layout.register, container, false);
        edtProv = (Button) v.findViewById(R.id.prov);
        edtKab = (Button) v.findViewById(R.id.kab);
        edtKec = (Button) v.findViewById(R.id.kec);
        edtKel = (Button) v.findViewById(R.id.kel);
        edtCab = (Button) v.findViewById(R.id.cab);
        edtMitra = (Button) v.findViewById(R.id.mitra);
        edtCib = (Button) v.findViewById(R.id.cib);
        edtNokt = (EditText) v.findViewById(R.id.noktp);
        edtNama = (EditText) v.findViewById(R.id.nama);
        edtAlamat = (EditText) v.findViewById(R.id.alamat);
        edtEmail = (EditText) v.findViewById(R.id.email);
        edtUserid = (EditText) v.findViewById(R.id.userid);
        edtPass = (EditText) v.findViewById(R.id.pass);
        edtCPass = (EditText) v.findViewById(R.id.cpass);
        btnRegister = (Button) v.findViewById(R.id.register);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        edtProv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog(1, "Pilih Provinsi");
            }
        });

        edtKab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog(2, prov);
            }
        });
        edtKec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog(3, kab);
            }
        });
        edtKel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog(4, kec);
            }
        });
        edtCab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog(5, kel);
            }
        });

        edtMitra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String a = kel + " " + cab;
                openDialog(6, a);
            }
        });

        edtCib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog(7, cif);
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtNokt.getText().toString().trim().isEmpty()) {
//                    Toast.makeText(getActivity(),"NIP Masih Kosong",Toast.LENGTH_LONG).show();
                    edtNokt.setError("Nip Masih Kosong");
                    edtNokt.requestFocus();
                    return;
                }
                if (edtNama.getText().toString().trim().isEmpty()) {
//                    Toast.makeText(getActivity(),"NIK Masih Kosong",Toast.LENGTH_LONG).show();
                    edtNama.setError("Nik Masih Kosong");
                    edtNama.requestFocus();
                    return;
                }

                if (edtAlamat.getText().toString().trim().isEmpty()) {
//                    Toast.makeText(getActivity(),"Telefon Masih Kosong",Toast.LENGTH_LONG).show();
                    edtAlamat.setError("Telefon Masih Kosong");
                    edtAlamat.requestFocus();
                    return;
                }
                if (edtEmail.getText().toString().trim().isEmpty()) {
//                    Toast.makeText(getActivity(),"Email Masih Kosong",Toast.LENGTH_LONG).show();
                    edtPass.setError("Email Masih Kosong");
                    edtPass.requestFocus();
                    return;
                }

                if (edtUserid.getText().toString().trim().isEmpty()) {
//                    Toast.makeText(getActivity(),"User ID Masih Kosong",Toast.LENGTH_LONG).show();
                    edtUserid.setError("UserID Masih Kosong");
                    edtUserid.requestFocus();
                    return;
                }


                if (edtPass.getText().toString().trim().isEmpty()) {
//                    Toast.makeText(getActivity(),"Password Masih Kosong",Toast.LENGTH_LONG).show();
                    edtPass.setError("Password Masih Kosong");
                    edtPass.requestFocus();
                    return;
                }


                if (edtCPass.getText().toString().trim().isEmpty()) {
//                    Toast.makeText(getActivity(),"Konfirmasi Password Masih Kosong",Toast.LENGTH_LONG).show();
                    edtCPass.setError("Konfirmasi Password Masih Kosong");
                    edtCPass.requestFocus();
                    return;
                }

                if (!edtPass.getText().toString().equals(edtCPass.getText().toString())) {
                    edtPass.setError("Password Tidak Sama");
                    edtCPass.setError("Password Tidak Sama");
                    edtCPass.requestFocus();
                    return;
                }

                HashMap<String, String> data = new HashMap<String, String>();
                data.put("cif", cif);
                data.put("cib", cib);
                data.put("nik", edtNama.getText().toString().trim());
                data.put("nip", edtNokt.getText().toString().trim());
                data.put("email", edtEmail.getText().toString().trim());
                data.put("telepon", edtAlamat.getText().toString().trim());
                data.put("uid", edtUserid.getText().toString().trim());
                data.put("pass", edtPass.getText().toString().trim());
                impRegister.register(data);
            }
        });
    }

    @Override
    public void sendData(int id, listData data) {
        switch (id) {
            case 1:
                edtProv.setText(data.getText());
                prov = data.getId();
                edtKab.setEnabled(true);
                break;
            case 2:
                kab = data.getId();
                edtKab.setText(data.getText());
                edtKec.setEnabled(true);

                break;
            case 3:
                kec = data.getId();
                edtKec.setText(data.getText());
                edtKel.setEnabled(true);
                break;
            case 4:
                kel = data.getId();
                edtKel.setText(data.getText());
                edtCab.setEnabled(true);
                break;
            case 5:
                cab = data.getId();
                edtCab.setText(data.getText());
                edtMitra.setEnabled(true);
                break;
            case 6:
                cif = data.getId();
                edtMitra.setText(data.getText());
                edtCib.setEnabled(true);
                break;
            case 7:
                cib = data.getId();
                edtCib.setText(data.getText());
                btnRegister.setEnabled(true);
            default:
                break;
        }
    }

    private void openDialog(int id, String title) {

        FragmentManager fm = getFragmentManager();
        FindProvinsi fp;
        switch (id) {
            case 2:
                fp = new FindProvinsi().newInstance(id, title);
                break;
            case 3:
                fp = new FindProvinsi().newInstance(id, title);
                break;
            case 4:
                fp = new FindProvinsi().newInstance(id, title);
                break;
            case 5:
                fp = new FindProvinsi().newInstance(id, title);
                break;
            default:
                fp = new FindProvinsi().newInstance(id, title);
        }

        fp.setTargetFragment(RegisterFragment.this, 200);
        fp.show(fm, "pilih");

    }


}
