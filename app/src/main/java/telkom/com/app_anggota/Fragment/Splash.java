package telkom.com.app_anggota.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import telkom.com.app_anggota.R;

/**
 * Created by sukri on 21/11/16.
 */

public class Splash extends Fragment {
    public Splash() {
    }


    public Splash newInstance() {
        Splash a = new Splash();
        return a;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.splash, container, false);
        return v;
    }
}
