package telkom.com.app_anggota.Profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import telkom.com.app_anggota.R;

/**
 * Created by sukri on 30/11/16.
 */

public class FPassword extends Fragment {

    public FPassword newInstance(String cif, String cib) {
        FPassword a = new FPassword();
        Bundle bundle = new Bundle();
        bundle.putString("cif", cif);
        bundle.putString("cib", cib);
        a.setArguments(bundle);
        return a;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_password, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

}
