package telkom.com.app_anggota.Profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import telkom.com.app_anggota.Adapter.AdapterProfile;
import telkom.com.app_anggota.Data.Profile;
import telkom.com.app_anggota.Helper.AppConfig;
import telkom.com.app_anggota.Helper.AppController;
import telkom.com.app_anggota.R;

/**
 * Created by sukri on 30/11/16.
 */

public class FProfile extends Fragment {
    private ListView listView;
    private ArrayList<Profile> data;
    private AdapterProfile adapterProfile;


    public FProfile newInstance(String cif, String cib) {
        FProfile a = new FProfile();
        Bundle bundle = new Bundle();
        bundle.putString("cif", cif);
        bundle.putString("cib", cib);
        a.setArguments(bundle);
        return a;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        listView = (ListView) view.findViewById(R.id.list);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        data = new ArrayList<>();
        adapterProfile = new AdapterProfile(getActivity(), data);
        listView.setAdapter(adapterProfile);
        fetchProfile();
    }

    private void fetchProfile() {
        String tag_string_req = "req_login";

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.PROFILE + "?cif=" + getArguments().getString("cif") + "&cib=" + getArguments().getString("cib"), new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject cjObj = new JSONObject(response);
                    if (cjObj.getBoolean("status")) {
                        data.clear();
                        JSONObject jObj = cjObj.getJSONObject("data");
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "NO CIB", jObj.getString("CIB")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "NO KARTU", jObj.getString("NOKARTU")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "NIP", jObj.getString("NIP")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "NAMA", jObj.getString("NAMA")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "KODE UNIT", jObj.getString("KI")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "PANGGILAN", jObj.getString("PANGGILAN")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "KELAMIN", jObj.getString("KL")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "TEMPAT LAHIR", jObj.getString("TMP_LAHIR")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "TANGGAL LAHIR", jObj.getString("TGL_LAHIR")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "TIPE ID", jObj.getString("ID")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "NO ID", jObj.getString("NO_ID")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "TANGGAL EXPIRED", jObj.getString("TGLEXPIRED")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "AGAMA", jObj.getString("AGAMA")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "PENDIDIKAN", jObj.getString("PENDIDIKAN")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "PEKERJAAN", jObj.getString("PEKERJAAN")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "JABATAN", jObj.getString("JABATAN")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "GOLONGAN", jObj.getString("GOLONGAN")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "ALAMAT", jObj.getString("ALAMAT")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "TELEPON", jObj.getString("TELEPON")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "HANDPHONE", jObj.getString("HANDPHONE")));
                        data.add(new Profile(R.drawable.ic_apps_black_24dp, "E MAIL", jObj.getString("E_MAIL")));
                    } else {
                        Toast.makeText(getActivity(), "Gagal Memuat data", Toast.LENGTH_SHORT).show();
                    }
                    adapterProfile.notifyDataSetChanged();
                } catch (JSONException e) {
//                    e.printStackTrace();
//                    Toast.makeText(getActivity(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e("login", "Login Error: " + error.getMessage());
                Toast.makeText(getActivity(),
                        "Terjadi kesalahan", Toast.LENGTH_LONG).show();

            }
        });

        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }
}
