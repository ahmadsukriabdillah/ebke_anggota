package telkom.com.app_anggota.AkunSaya;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import telkom.com.app_anggota.Adapter.AdapterPiutang;
import telkom.com.app_anggota.Data.DetailPiutang;
import telkom.com.app_anggota.Data.Piutang;
import telkom.com.app_anggota.Helper.AppConfig;
import telkom.com.app_anggota.Helper.AppController;
import telkom.com.app_anggota.Helper.CallDialog;
import telkom.com.app_anggota.R;

/**
 * Created by sukri on 22/11/16.
 */

public class FPiutang extends Fragment {
    private ArrayList<Piutang> piutangs;
    private AdapterPiutang adapterPiutang;
    private ListView list2;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private LinearLayout empty;
    private int tray = 0;

    public FPiutang() {

    }

    public FPiutang newInstance(String cif, String cib) {
        FPiutang fPiutang = new FPiutang();
        Bundle a = new Bundle();
        a.putString("cib", cib);
        a.putString("cif", cif);
        fPiutang.setArguments(a);
        return fPiutang;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_deposito, container, false);
        list2 = (ListView) v.findViewById(R.id.list);
        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe);
        empty = (LinearLayout) v.findViewById(R.id.empty);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        piutangs = new ArrayList<>();
        FragmentManager fm = getFragmentManager();
        adapterPiutang = new AdapterPiutang(getActivity(), piutangs, fm);
        list2.setAdapter(adapterPiutang);
        mSwipeRefreshLayout.setRefreshing(true);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(true);
                mSwipeRefreshLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        fetchDetail();
                    }
                }, 2000);
            }
        });

        mSwipeRefreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                fetchDetail();
            }
        }, 2000);
//        list2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                FragmentManager fm = getFragmentManager();
//                PiutDetFragment piutDetFragment = new PiutDetFragment().newInstance(piutangs.get(position).getMutasi());
//                piutDetFragment.show(fm,"Detail");
//            }
//        });


    }

    private void fetchDetail() {
        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.FETCH_PIUT + "?cif=" + getArguments().getString("cif") + "&cib=" + getArguments().getString("cib"), new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                Log.d("login", "Login Response: " + response.toString());
//                Log.d("login", "Login Response: " + AppConfig.FETCH_PIUT+"?cif="+getArguments().getString("cif")+"&cib="+getArguments().getString("cib"));

                try {
                    piutangs.clear();
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getBoolean("status")) {
                        list2.setVisibility(View.VISIBLE);
                        empty.setVisibility(View.INVISIBLE);
                        JSONArray simp = jsonObject.getJSONArray("data");
                        for (int i = 0; i < simp.length(); i++) {
                            JSONObject obj = simp.getJSONObject(i);
                            Piutang data = new Piutang();
                            data.setACC(obj.getString("ACC"));
                            data.setNOPINJ(obj.getString("NOPINJ"));
                            data.setOUTSTANDING(obj.getString("OUTSTANDING"));
                            data.setKETERANGAN(obj.getString("KETERANGAN"));
                            data.setPLAFOND(obj.getString("PLAFOND"));
                            data.setTGLAKAD(obj.getString("TGLAKAD"));
                            data.setTGLANGS(obj.getString("TGLANGS"));
                            data.setJW(obj.getString("JW"));
                            data.setJATUHTEMPO(obj.getString("JATUHTEMPO"));
                            data.setSBP(obj.getString("SBP"));
                            data.setKE(obj.getString("KE"));
                            data.setCALL(obj.getString("COLL"));
                            data.setANGSPOKOK(obj.getString("ANGSPOKOK"));
                            data.setANGSBUNGA(obj.getString("ANGSBUNGA"));
                            data.setANGSURAN(obj.getString("ANGSURAN"));
                            JSONArray jsonArray = obj.getJSONArray("mutasi");
                            ArrayList<DetailPiutang> listMutasi = new ArrayList<>();
                            for (int j = 0; j < jsonArray.length(); j++) {
                                JSONObject kobj = jsonArray.getJSONObject(j);
                                DetailPiutang detailPiutang = new DetailPiutang();
                                detailPiutang.setTANGGAL(kobj.getString("TANGGAL"));
                                detailPiutang.setKETERANGAN(kobj.getString("KETERANGAN"));
                                detailPiutang.setKE(kobj.getString("KE"));
                                detailPiutang.setSBP(kobj.getString("SBP"));
                                detailPiutang.setCOLL(kobj.getString("COLL"));
                                detailPiutang.setANGSPOKOK(kobj.getString("ANGSPOKOK"));
                                detailPiutang.setANGSBUNGA(kobj.getString("ANGSBUNGA"));
                                detailPiutang.setANGSURAN(kobj.getString("ANGSURAN"));
                                detailPiutang.setDENDA(kobj.getString("DENDA"));
                                detailPiutang.setPENALTY(kobj.getString("PENALTY"));
                                detailPiutang.setOUTSTANDING(kobj.getString("OUTSTANDING"));
                                listMutasi.add(detailPiutang);
                            }
                            data.setMutasi(listMutasi);
                            piutangs.add(data);
                        }

                    } else {
                        list2.setVisibility(View.INVISIBLE);
                        empty.setVisibility(View.VISIBLE);
                    }
                    adapterPiutang.notifyDataSetChanged();
                    mSwipeRefreshLayout.setRefreshing(false);


                } catch (JSONException e) {
//                    e.printStackTrace();
//                    Toast.makeText(getActivity(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e("login", "Login Error: " + error.getMessage());
//                Toast.makeText(getActivity(),
//                        "Terjadi Kesalahan", Toast.LENGTH_LONG).show();
                if(tray < 5){
                    tray++;
                    CallDialog.showMassage(getActivity(),"Erorr","Terjadi Kesalahan Coba Lagi");
                }else{
                    CallDialog.showMassage(getActivity(),"Erorr","Server Bermasalah coba Beberapa saat kemudian");
                }

            }
        });

        AppController.getInstance().addToRequestQueue(strReq, "get Piutang");

    }


}
