package telkom.com.app_anggota.AkunSaya;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import telkom.com.app_anggota.Adapter.AdapterDeposito;
import telkom.com.app_anggota.Data.Deposito;
import telkom.com.app_anggota.Data.DetailDeposito;
import telkom.com.app_anggota.Helper.AppConfig;
import telkom.com.app_anggota.Helper.AppController;
import telkom.com.app_anggota.Helper.CallDialog;
import telkom.com.app_anggota.R;

/**
 * Created by sukri on 22/11/16.
 */

public class FDeposit extends Fragment {

    private ArrayList<Deposito> depositos;
    private AdapterDeposito adapterDeposito;
    private ListView list2;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private LinearLayout empty;
    private int tray = 0;

    public FDeposit() {

    }

    public FDeposit newInstance(String cif, String cib) {
        FDeposit fDeposit = new FDeposit();
        Bundle a = new Bundle();
        a.putString("cib", cib);
        a.putString("cif", cif);
        fDeposit.setArguments(a);
        return fDeposit;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_deposito, container, false);
        list2 = (ListView) v.findViewById(R.id.list);
        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe);
        empty = (LinearLayout) v.findViewById(R.id.empty);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        depositos = new ArrayList<>();
        FragmentManager fm = getFragmentManager();
        adapterDeposito = new AdapterDeposito(getActivity(), depositos, fm);
        list2.setAdapter(adapterDeposito);
        mSwipeRefreshLayout.setRefreshing(true);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(true);
                mSwipeRefreshLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        fetchDetail();
                    }
                }, 2000);
            }
        });

        mSwipeRefreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                fetchDetail();
            }
        }, 2000);


    }

    private void fetchDetail() {

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.FETCH_DEP + "?cif=" + getArguments().getString("cif") + "&cib=" + getArguments().getString("cib"), new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
//                Log.d("login", "Login Response: " + response.toString());
//                Log.d("login", "Login Response: " + AppConfig.FETCH_DEP+"?cif="+getArguments().getString("cif")+"&cib="+getArguments().getString("cib"));

                try {
                    depositos.clear();
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getBoolean("status")) {
                        list2.setVisibility(View.VISIBLE);
                        empty.setVisibility(View.INVISIBLE);
                        JSONArray simp = jsonObject.getJSONArray("data");
                        for (int i = 0; i < simp.length(); i++) {
                            JSONObject obj = simp.getJSONObject(i);
                            Deposito data = new Deposito();
                            data.setACC(obj.getString("ACC"));
                            data.setNODEP(obj.getString("NODEP"));
                            data.setNOMINAL(obj.getString("NOMINAL"));
                            data.setKETERANGAN(obj.getString("KETERANGAN"));
                            data.setTGLBUKA(obj.getString("TGLBUKA"));
                            data.setTGLCAIR(obj.getString("TGLCAIR"));
                            data.setTGLAKTIF(obj.getString("TGLAKTIF"));
                            data.setBUNGA(obj.getString("BUNGA"));
                            data.setPAJAK(obj.getString("PAJAK"));
                            data.setJW(obj.getString("JW"));

                            JSONArray jsonArray = obj.getJSONArray("mutasi");
                            ArrayList<DetailDeposito> listMutasi = new ArrayList<>();
                            for (int j = 0; j < jsonArray.length(); j++) {
                                JSONObject kobj = jsonArray.getJSONObject(j);
                                DetailDeposito detailDeposito = new DetailDeposito();
                                detailDeposito.setTANGGAL(kobj.getString("TANGGAL"));
                                detailDeposito.setKETERANGAN(kobj.getString("KETERANGAN"));
                                detailDeposito.setBERITA(kobj.getString("BERITA"));
                                detailDeposito.setBUNGA(kobj.getString("BUNGA"));
                                detailDeposito.setPAJAK(kobj.getString("PAJAK"));
                                detailDeposito.setNOMINAL(kobj.getString("NOMINAL"));
                                listMutasi.add(detailDeposito);

                            }
                            data.setMutasi(listMutasi);
                            depositos.add(data);
                        }

                    } else {
                        list2.setVisibility(View.INVISIBLE);
                        empty.setVisibility(View.VISIBLE);
                    }
                    adapterDeposito.notifyDataSetChanged();
                    mSwipeRefreshLayout.setRefreshing(false);


                } catch (JSONException e) {
//                    e.printStackTrace();
//                    Toast.makeText(getActivity(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e("login", "Login Error: " + error.getMessage());
//                Toast.makeText(getActivity(),
//                        "Terjadi Kesalahan", Toast.LENGTH_LONG).show();

                if(tray < 5){
                    tray++;
                    CallDialog.showMassage(getActivity(),"Erorr","Terjadi Kesalahan Coba Lagi");
                }else{
                    CallDialog.showMassage(getActivity(),"Erorr","Server Bermasalah coba Beberapa saat kemudian");
                }

            }
        });

        AppController.getInstance().addToRequestQueue(strReq, "get Piutang");

    }

}
