package telkom.com.app_anggota.AkunSaya;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import telkom.com.app_anggota.Adapter.AdapterSimpanan;
import telkom.com.app_anggota.Data.ListSimpanan;
import telkom.com.app_anggota.Data.Simpanan;
import telkom.com.app_anggota.Helper.AppConfig;
import telkom.com.app_anggota.Helper.AppController;
import telkom.com.app_anggota.Helper.CallDialog;
import telkom.com.app_anggota.R;

/**
 * Created by sukri on 22/11/16.
 */

public class FSimpanan extends Fragment {
    private ArrayList<Simpanan> simpanan;
    private AdapterSimpanan adapterSimpanan;
    private ListView list2;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private LinearLayout empty;
    private int tray = 0;

    public FSimpanan() {

    }

    public FSimpanan newInstance(String cif, String cib) {
        FSimpanan fSimpanan = new FSimpanan();
        Bundle a = new Bundle();
        a.putString("cib", cib);
        a.putString("cif", cif);
        fSimpanan.setArguments(a);
        return fSimpanan;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_deposito, container, false);
        if (savedInstanceState != null){
            return null;
        }
        list2 = (ListView) v.findViewById(R.id.list);
        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe);
        empty = (LinearLayout) v.findViewById(R.id.empty);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState == null) {
            simpanan = new ArrayList<>();
            FragmentManager fm = getFragmentManager();
            adapterSimpanan = new AdapterSimpanan(getActivity(), simpanan, fm);
            list2.setAdapter(adapterSimpanan);
            mSwipeRefreshLayout.setRefreshing(true);
            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    mSwipeRefreshLayout.setRefreshing(true);
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            fetchDetail();
                        }
                    }, 2000);
                }
            });
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    fetchDetail();
                }
            }, 2000);

        }else{
            simpanan = savedInstanceState.getParcelableArrayList("simpanan");
            FragmentManager fm = getFragmentManager();
            adapterSimpanan = new AdapterSimpanan(getActivity(), simpanan, fm);
            list2.setAdapter(adapterSimpanan);
            adapterSimpanan.notifyDataSetChanged();
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("simpanan",simpanan);
    }

    private void fetchDetail() {
        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.FETCH_SIMP + "?cif=" + getArguments().getString("cif") + "&cib=" + getArguments().getString("cib"), new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("login", "Login Response: " + response.toString());
                Log.d("login", "Login Response: " + AppConfig.FETCH_SIMP + "?cif=" + getArguments().getString("cif") + "&cib=" + getArguments().getString("cib"));

                try {
                    simpanan.clear();
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getBoolean("status")) {
                        list2.setVisibility(View.VISIBLE);
                        empty.setVisibility(View.INVISIBLE);
                        JSONArray simp = jsonObject.getJSONArray("data");
                        for (int i = 0; i < simp.length(); i++) {
                            JSONObject obj = simp.getJSONObject(i);
                            Simpanan data = new Simpanan();
                            data.setAcc(obj.getString("ACC"));
                            data.setNosimp(obj.getString("NOSIMP"));
                            data.setSaldo(obj.getString("SALDO"));
                            data.setKredit(obj.getString("KREDIT"));
                            data.setDebet(obj.getString("DEBET"));
                            data.setSaldo(obj.getString("SALDO"));
                            data.setTglaktif(obj.getString("TGLAKTIF"));
                            data.setTglbeban(obj.getString("TGLBEBAN"));
                            data.setTglbuka(obj.getString("TGLBUKA"));
                            data.setKeterangan(obj.getString("KETERANGAN"));
                            JSONArray jsonArray = obj.getJSONArray("mutasi");
                            ArrayList<ListSimpanan> listMutasi = new ArrayList<>();
                            for (int j = 0; j < jsonArray.length(); j++) {
                                JSONObject kobj = jsonArray.getJSONObject(j);
                                ListSimpanan a = new ListSimpanan(j, kobj.getString("TGLSETOR"), kobj.getString("NOSIMP"), kobj.getString("KETERANGAN"),
                                        kobj.getString("SALDO"), kobj.getString("DEBET"), kobj.getString("KREDIT"));
                                a.setBerita(kobj.getString("BERITA"));
                                listMutasi.add(a);
                            }
                            data.setSimp(listMutasi);
                            simpanan.add(data);
                        }
                    } else {
                        list2.setVisibility(View.INVISIBLE);
                        empty.setVisibility(View.VISIBLE);
                    }
                    adapterSimpanan.notifyDataSetChanged();
                    mSwipeRefreshLayout.setRefreshing(false);


                } catch (JSONException e) {
//                    e.printStackTrace();
//                    Toast.makeText(getActivity(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e("login", "Login Error: " + error.getMessage());
//                Toast.makeText(getActivity(),
//                        "Terjadi Kesalahan", Toast.LENGTH_LONG).show();
                if(tray < 5){
                    tray++;
                    CallDialog.showMassage(getActivity(),"Erorr","Terjadi Kesalahan Coba Lagi");
                }else{
                    CallDialog.showMassage(getActivity(),"Erorr","Server Bermasalah coba Beberapa saat kemudian");
                }

            }
        });

        AppController.getInstance().addToRequestQueue(strReq, "get Piutang");

    }
}
