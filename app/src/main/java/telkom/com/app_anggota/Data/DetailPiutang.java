package telkom.com.app_anggota.Data;

/**
 * Created by sukri on 01/12/16.
 */

public class DetailPiutang {

    private String iDE;
    private String nTRANS;
    private String tANGGAL;
    private String uID;
    private String nOPINJ;
    private String tGLBEBAN;
    private String tGLANGS;
    private String pLAFOND;
    private String oUTSTANDING;
    private String aNGSPOKOK;
    private String aNGSBUNGA;
    private String aNGSURAN;
    private String dENDA;
    private String pENALTY;
    private String aNGSPOKOK_CONVR;
    private String aNGSBUNGA_CONVR;
    private String aNGSURAN_CONVR;
    private String nOMANGSPOKOK;
    private String nOMANGSBUNGA;
    private String nOMANGSURAN;
    private String nOMDENDA;
    private String nOMPENALTY;
    private String nOMANGSPOKOK_CONVR;
    private String nOMANGSBUNGA_CONVR;
    private String nOMANGSURAN_CONVR;
    private String sBP;
    private String kE;
    private String jBL;
    private String cOLL;
    private String bLN;
    private String hARI;
    private String kETERANGAN;
    private String bERITA;
    private String sT;
    private String kU;
    private String cIF;

    /**
     * No args constructor for use in serialization
     */
    public DetailPiutang() {
    }

    /**
     * @param pLAFOND
     * @param aNGSPOKOK
     * @param kU
     * @param aNGSURAN_CONVR
     * @param nOMANGSPOKOK_CONVR
     * @param uID
     * @param nOMDENDA
     * @param bERITA
     * @param tGLBEBAN
     * @param aNGSPOKOK_CONVR
     * @param nOMANGSBUNGA_CONVR
     * @param cOLL
     * @param nOMANGSURAN
     * @param aNGSBUNGA
     * @param bLN
     * @param kETERANGAN
     * @param aNGSBUNGA_CONVR
     * @param nOMANGSURAN_CONVR
     * @param cIF
     * @param jBL
     * @param aNGSURAN
     * @param hARI
     * @param pENALTY
     * @param sBP
     * @param oUTSTANDING
     * @param nOMANGSBUNGA
     * @param nOPINJ
     * @param tANGGAL
     * @param nTRANS
     * @param nOMPENALTY
     * @param kE
     * @param nOMANGSPOKOK
     * @param tGLANGS
     * @param sT
     * @param iDE
     * @param dENDA
     */
    public DetailPiutang(String iDE, String nTRANS, String tANGGAL, String uID, String nOPINJ, String tGLBEBAN, String tGLANGS, String pLAFOND, String oUTSTANDING, String aNGSPOKOK, String aNGSBUNGA, String aNGSURAN, String dENDA, String pENALTY, String aNGSPOKOK_CONVR, String aNGSBUNGA_CONVR, String aNGSURAN_CONVR, String nOMANGSPOKOK, String nOMANGSBUNGA, String nOMANGSURAN, String nOMDENDA, String nOMPENALTY, String nOMANGSPOKOK_CONVR, String nOMANGSBUNGA_CONVR, String nOMANGSURAN_CONVR, String sBP, String kE, String jBL, String cOLL, String bLN, String hARI, String kETERANGAN, String bERITA, String sT, String kU, String cIF) {
        this.iDE = iDE;
        this.nTRANS = nTRANS;
        this.tANGGAL = tANGGAL;
        this.uID = uID;
        this.nOPINJ = nOPINJ;
        this.tGLBEBAN = tGLBEBAN;
        this.tGLANGS = tGLANGS;
        this.pLAFOND = pLAFOND;
        this.oUTSTANDING = oUTSTANDING;
        this.aNGSPOKOK = aNGSPOKOK;
        this.aNGSBUNGA = aNGSBUNGA;
        this.aNGSURAN = aNGSURAN;
        this.dENDA = dENDA;
        this.pENALTY = pENALTY;
        this.aNGSPOKOK_CONVR = aNGSPOKOK_CONVR;
        this.aNGSBUNGA_CONVR = aNGSBUNGA_CONVR;
        this.aNGSURAN_CONVR = aNGSURAN_CONVR;
        this.nOMANGSPOKOK = nOMANGSPOKOK;
        this.nOMANGSBUNGA = nOMANGSBUNGA;
        this.nOMANGSURAN = nOMANGSURAN;
        this.nOMDENDA = nOMDENDA;
        this.nOMPENALTY = nOMPENALTY;
        this.nOMANGSPOKOK_CONVR = nOMANGSPOKOK_CONVR;
        this.nOMANGSBUNGA_CONVR = nOMANGSBUNGA_CONVR;
        this.nOMANGSURAN_CONVR = nOMANGSURAN_CONVR;
        this.sBP = sBP;
        this.kE = kE;
        this.jBL = jBL;
        this.cOLL = cOLL;
        this.bLN = bLN;
        this.hARI = hARI;
        this.kETERANGAN = kETERANGAN;
        this.bERITA = bERITA;
        this.sT = sT;
        this.kU = kU;
        this.cIF = cIF;
    }

    /**
     * @return The iDE
     */
    public String getIDE() {
        return iDE;
    }

    /**
     * @param iDE The IDE
     */
    public void setIDE(String iDE) {
        this.iDE = iDE;
    }

    /**
     * @return The nTRANS
     */
    public String getNTRANS() {
        return nTRANS;
    }

    /**
     * @param nTRANS The NTRANS
     */
    public void setNTRANS(String nTRANS) {
        this.nTRANS = nTRANS;
    }

    /**
     * @return The tANGGAL
     */
    public String getTANGGAL() {
        return tANGGAL;
    }

    /**
     * @param tANGGAL The TANGGAL
     */
    public void setTANGGAL(String tANGGAL) {
        this.tANGGAL = tANGGAL;
    }

    /**
     * @return The uID
     */
    public String getUID() {
        return uID;
    }

    /**
     * @param uID The UID
     */
    public void setUID(String uID) {
        this.uID = uID;
    }

    /**
     * @return The nOPINJ
     */
    public String getNOPINJ() {
        return nOPINJ;
    }

    /**
     * @param nOPINJ The NOPINJ
     */
    public void setNOPINJ(String nOPINJ) {
        this.nOPINJ = nOPINJ;
    }

    /**
     * @return The tGLBEBAN
     */
    public Object getTGLBEBAN() {
        return tGLBEBAN;
    }

    /**
     * @param tGLBEBAN The TGLBEBAN
     */
    public void setTGLBEBAN(String tGLBEBAN) {
        this.tGLBEBAN = tGLBEBAN;
    }

    /**
     * @return The tGLANGS
     */
    public String getTGLANGS() {
        return tGLANGS;
    }

    /**
     * @param tGLANGS The TGLANGS
     */
    public void setTGLANGS(String tGLANGS) {
        this.tGLANGS = tGLANGS;
    }

    /**
     * @return The pLAFOND
     */
    public String getPLAFOND() {
        return pLAFOND;
    }

    /**
     * @param pLAFOND The PLAFOND
     */
    public void setPLAFOND(String pLAFOND) {
        this.pLAFOND = pLAFOND;
    }

    /**
     * @return The oUTSTANDING
     */
    public String getOUTSTANDING() {
        return oUTSTANDING;
    }

    /**
     * @param oUTSTANDING The OUTSTANDING
     */
    public void setOUTSTANDING(String oUTSTANDING) {
        this.oUTSTANDING = oUTSTANDING;
    }

    /**
     * @return The aNGSPOKOK
     */
    public String getANGSPOKOK() {
        return aNGSPOKOK;
    }

    /**
     * @param aNGSPOKOK The ANGSPOKOK
     */
    public void setANGSPOKOK(String aNGSPOKOK) {
        this.aNGSPOKOK = aNGSPOKOK;
    }

    /**
     * @return The aNGSBUNGA
     */
    public String getANGSBUNGA() {
        return aNGSBUNGA;
    }

    /**
     * @param aNGSBUNGA The ANGSBUNGA
     */
    public void setANGSBUNGA(String aNGSBUNGA) {
        this.aNGSBUNGA = aNGSBUNGA;
    }

    /**
     * @return The aNGSURAN
     */
    public String getANGSURAN() {
        return aNGSURAN;
    }

    /**
     * @param aNGSURAN The ANGSURAN
     */
    public void setANGSURAN(String aNGSURAN) {
        this.aNGSURAN = aNGSURAN;
    }

    /**
     * @return The dENDA
     */
    public String getDENDA() {
        return dENDA;
    }

    /**
     * @param dENDA The DENDA
     */
    public void setDENDA(String dENDA) {
        this.dENDA = dENDA;
    }

    /**
     * @return The pENALTY
     */
    public String getPENALTY() {
        return pENALTY;
    }

    /**
     * @param pENALTY The PENALTY
     */
    public void setPENALTY(String pENALTY) {
        this.pENALTY = pENALTY;
    }

    /**
     * @return The aNGSPOKOK_CONVR
     */
    public String getANGSPOKOK_CONVR() {
        return aNGSPOKOK_CONVR;
    }

    /**
     * @param aNGSPOKOK_CONVR The ANGSPOKOK_CONVR
     */
    public void setANGSPOKOK_CONVR(String aNGSPOKOK_CONVR) {
        this.aNGSPOKOK_CONVR = aNGSPOKOK_CONVR;
    }

    /**
     * @return The aNGSBUNGA_CONVR
     */
    public String getANGSBUNGA_CONVR() {
        return aNGSBUNGA_CONVR;
    }

    /**
     * @param aNGSBUNGA_CONVR The ANGSBUNGA_CONVR
     */
    public void setANGSBUNGA_CONVR(String aNGSBUNGA_CONVR) {
        this.aNGSBUNGA_CONVR = aNGSBUNGA_CONVR;
    }

    /**
     * @return The aNGSURAN_CONVR
     */
    public String getANGSURAN_CONVR() {
        return aNGSURAN_CONVR;
    }

    /**
     * @param aNGSURAN_CONVR The ANGSURAN_CONVR
     */
    public void setANGSURAN_CONVR(String aNGSURAN_CONVR) {
        this.aNGSURAN_CONVR = aNGSURAN_CONVR;
    }

    /**
     * @return The nOMANGSPOKOK
     */
    public String getNOMANGSPOKOK() {
        return nOMANGSPOKOK;
    }

    /**
     * @param nOMANGSPOKOK The NOMANGSPOKOK
     */
    public void setNOMANGSPOKOK(String nOMANGSPOKOK) {
        this.nOMANGSPOKOK = nOMANGSPOKOK;
    }

    /**
     * @return The nOMANGSBUNGA
     */
    public String getNOMANGSBUNGA() {
        return nOMANGSBUNGA;
    }

    /**
     * @param nOMANGSBUNGA The NOMANGSBUNGA
     */
    public void setNOMANGSBUNGA(String nOMANGSBUNGA) {
        this.nOMANGSBUNGA = nOMANGSBUNGA;
    }

    /**
     * @return The nOMANGSURAN
     */
    public String getNOMANGSURAN() {
        return nOMANGSURAN;
    }

    /**
     * @param nOMANGSURAN The NOMANGSURAN
     */
    public void setNOMANGSURAN(String nOMANGSURAN) {
        this.nOMANGSURAN = nOMANGSURAN;
    }

    /**
     * @return The nOMDENDA
     */
    public String getNOMDENDA() {
        return nOMDENDA;
    }

    /**
     * @param nOMDENDA The NOMDENDA
     */
    public void setNOMDENDA(String nOMDENDA) {
        this.nOMDENDA = nOMDENDA;
    }

    /**
     * @return The nOMPENALTY
     */
    public String getNOMPENALTY() {
        return nOMPENALTY;
    }

    /**
     * @param nOMPENALTY The NOMPENALTY
     */
    public void setNOMPENALTY(String nOMPENALTY) {
        this.nOMPENALTY = nOMPENALTY;
    }

    /**
     * @return The nOMANGSPOKOK_CONVR
     */
    public String getNOMANGSPOKOK_CONVR() {
        return nOMANGSPOKOK_CONVR;
    }

    /**
     * @param nOMANGSPOKOK_CONVR The NOMANGSPOKOK_CONVR
     */
    public void setNOMANGSPOKOK_CONVR(String nOMANGSPOKOK_CONVR) {
        this.nOMANGSPOKOK_CONVR = nOMANGSPOKOK_CONVR;
    }

    /**
     * @return The nOMANGSBUNGA_CONVR
     */
    public String getNOMANGSBUNGA_CONVR() {
        return nOMANGSBUNGA_CONVR;
    }

    /**
     * @param nOMANGSBUNGA_CONVR The NOMANGSBUNGA_CONVR
     */
    public void setNOMANGSBUNGA_CONVR(String nOMANGSBUNGA_CONVR) {
        this.nOMANGSBUNGA_CONVR = nOMANGSBUNGA_CONVR;
    }

    /**
     * @return The nOMANGSURAN_CONVR
     */
    public String getNOMANGSURAN_CONVR() {
        return nOMANGSURAN_CONVR;
    }

    /**
     * @param nOMANGSURAN_CONVR The NOMANGSURAN_CONVR
     */
    public void setNOMANGSURAN_CONVR(String nOMANGSURAN_CONVR) {
        this.nOMANGSURAN_CONVR = nOMANGSURAN_CONVR;
    }

    /**
     * @return The sBP
     */
    public String getSBP() {
        return sBP;
    }

    /**
     * @param sBP The SBP
     */
    public void setSBP(String sBP) {
        this.sBP = sBP;
    }

    /**
     * @return The kE
     */
    public String getKE() {
        return kE;
    }

    /**
     * @param kE The KE
     */
    public void setKE(String kE) {
        this.kE = kE;
    }

    /**
     * @return The jBL
     */
    public String getJBL() {
        return jBL;
    }

    /**
     * @param jBL The JBL
     */
    public void setJBL(String jBL) {
        this.jBL = jBL;
    }

    /**
     * @return The cOLL
     */
    public String getCOLL() {
        return cOLL;
    }

    /**
     * @param cOLL The COLL
     */
    public void setCOLL(String cOLL) {
        this.cOLL = cOLL;
    }

    /**
     * @return The bLN
     */
    public String getBLN() {
        return bLN;
    }

    /**
     * @param bLN The BLN
     */
    public void setBLN(String bLN) {
        this.bLN = bLN;
    }

    /**
     * @return The hARI
     */
    public String getHARI() {
        return hARI;
    }

    /**
     * @param hARI The HARI
     */
    public void setHARI(String hARI) {
        this.hARI = hARI;
    }

    /**
     * @return The kETERANGAN
     */
    public String getKETERANGAN() {
        return kETERANGAN;
    }

    /**
     * @param kETERANGAN The KETERANGAN
     */
    public void setKETERANGAN(String kETERANGAN) {
        this.kETERANGAN = kETERANGAN;
    }

    /**
     * @return The bERITA
     */
    public String getBERITA() {
        return bERITA;
    }

    /**
     * @param bERITA The BERITA
     */
    public void setBERITA(String bERITA) {
        this.bERITA = bERITA;
    }

    /**
     * @return The sT
     */
    public String getST() {
        return sT;
    }

    /**
     * @param sT The ST
     */
    public void setST(String sT) {
        this.sT = sT;
    }

    /**
     * @return The kU
     */
    public String getKU() {
        return kU;
    }

    /**
     * @param kU The KU
     */
    public void setKU(String kU) {
        this.kU = kU;
    }

    /**
     * @return The cIF
     */
    public String getCIF() {
        return cIF;
    }

    /**
     * @param cIF The CIF
     */
    public void setCIF(String cIF) {
        this.cIF = cIF;
    }

}
