package telkom.com.app_anggota.Data;

/**
 * Created by sukri on 02/12/16.
 */
public class DetailDeposito {

    private String iDE;
    private String tANGGAL;
    private String nTRANS;
    private String nODEP;
    private String bILYET;
    private String kETERANGAN;
    private String bERITA;
    private String bUNGA;
    private String pAJAK;
    private String pENALTY;
    private String nOMINAL;
    private String tNOMINAL;
    private String kU;
    private String cIF;
    private String uID;

    public DetailDeposito() {
    }

    /**
     * @return The iDE
     */
    public String getIDE() {
        return iDE;
    }

    /**
     * @param iDE The IDE
     */
    public void setIDE(String iDE) {
        this.iDE = iDE;
    }

    /**
     * @return The tANGGAL
     */
    public String getTANGGAL() {
        return tANGGAL;
    }

    /**
     * @param tANGGAL The TANGGAL
     */
    public void setTANGGAL(String tANGGAL) {
        this.tANGGAL = tANGGAL;
    }

    /**
     * @return The nTRANS
     */
    public String getNTRANS() {
        return nTRANS;
    }

    /**
     * @param nTRANS The NTRANS
     */
    public void setNTRANS(String nTRANS) {
        this.nTRANS = nTRANS;
    }

    /**
     * @return The nODEP
     */
    public String getNODEP() {
        return nODEP;
    }

    /**
     * @param nODEP The NODEP
     */
    public void setNODEP(String nODEP) {
        this.nODEP = nODEP;
    }

    /**
     * @return The bILYET
     */
    public String getBILYET() {
        return bILYET;
    }

    /**
     * @param bILYET The BILYET
     */
    public void setBILYET(String bILYET) {
        this.bILYET = bILYET;
    }

    /**
     * @return The kETERANGAN
     */
    public String getKETERANGAN() {
        return kETERANGAN;
    }

    /**
     * @param kETERANGAN The KETERANGAN
     */
    public void setKETERANGAN(String kETERANGAN) {
        this.kETERANGAN = kETERANGAN;
    }

    /**
     * @return The bERITA
     */
    public String getBERITA() {
        return bERITA;
    }

    /**
     * @param bERITA The BERITA
     */
    public void setBERITA(String bERITA) {
        this.bERITA = bERITA;
    }

    /**
     * @return The bUNGA
     */
    public String getBUNGA() {
        return bUNGA;
    }

    /**
     * @param bUNGA The BUNGA
     */
    public void setBUNGA(String bUNGA) {
        this.bUNGA = bUNGA;
    }

    /**
     * @return The pAJAK
     */
    public String getPAJAK() {
        return pAJAK;
    }

    /**
     * @param pAJAK The PAJAK
     */
    public void setPAJAK(String pAJAK) {
        this.pAJAK = pAJAK;
    }

    /**
     * @return The pENALTY
     */
    public String getPENALTY() {
        return pENALTY;
    }

    /**
     * @param pENALTY The PENALTY
     */
    public void setPENALTY(String pENALTY) {
        this.pENALTY = pENALTY;
    }

    /**
     * @return The nOMINAL
     */
    public String getNOMINAL() {
        return nOMINAL;
    }

    /**
     * @param nOMINAL The NOMINAL
     */
    public void setNOMINAL(String nOMINAL) {
        this.nOMINAL = nOMINAL;
    }

    /**
     * @return The tNOMINAL
     */
    public String getTNOMINAL() {
        return tNOMINAL;
    }

    /**
     * @param tNOMINAL The TNOMINAL
     */
    public void setTNOMINAL(String tNOMINAL) {
        this.tNOMINAL = tNOMINAL;
    }

    /**
     * @return The kU
     */
    public String getKU() {
        return kU;
    }

    /**
     * @param kU The KU
     */
    public void setKU(String kU) {
        this.kU = kU;
    }

    /**
     * @return The cIF
     */
    public String getCIF() {
        return cIF;
    }

    /**
     * @param cIF The CIF
     */
    public void setCIF(String cIF) {
        this.cIF = cIF;
    }

    /**
     * @return The uID
     */
    public String getUID() {
        return uID;
    }

    /**
     * @param uID The UID
     */
    public void setUID(String uID) {
        this.uID = uID;
    }
}