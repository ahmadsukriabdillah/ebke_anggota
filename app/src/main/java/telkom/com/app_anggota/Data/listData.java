package telkom.com.app_anggota.Data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sukri on 22/11/16.
 */

public class listData implements Parcelable {
    public static final Creator<listData> CREATOR = new Creator<listData>() {
        @Override
        public listData createFromParcel(Parcel source) {
            return new listData(source);
        }

        @Override
        public listData[] newArray(int size) {
            return new listData[size];
        }
    };
    private String id;
    private String text;

    public listData(String id, String text) {
        this.id = id;
        this.text = text;
    }

    public listData() {
    }

    protected listData(Parcel in) {
        this.id = in.readString();
        this.text = in.readString();
    }

    @Override
    public String toString() {
        return this.text;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.text);
    }
}
