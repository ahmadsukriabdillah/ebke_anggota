package telkom.com.app_anggota.Data;

import java.util.ArrayList;


/**
 * Created by sukri on 02/12/16.
 */

public class Deposito {

    private String nODEP;
    private String aCC;
    private String cIB;
    private String bILYET;
    private String tGLBUKA;
    private String tGLAKTIF;
    private String tGLCAIR;
    private String rEKST;
    private String nOSIMP;
    private String jW;
    private String pB;
    private String rO;
    private String bGA;
    private String bUNGA;
    private String pJK;
    private String pAJAK;
    private String pEN;
    private String pENALTY;
    private String nOMINAL;
    private String sT;
    private String tGLAPV;
    private String uIDAPV;
    private String uID;
    private String kU;
    private String cIF;
    private String aCCBB;
    private String kETERANGAN;
    private String gOLONGAN;
    private ArrayList<DetailDeposito> mutasi;

    public Deposito() {
    }

    /**
     * @return The nODEP
     */
    public String getNODEP() {
        return nODEP;
    }

    /**
     * @param nODEP The NODEP
     */
    public void setNODEP(String nODEP) {
        this.nODEP = nODEP;
    }

    /**
     * @return The aCC
     */
    public String getACC() {
        return aCC;
    }

    /**
     * @param aCC The ACC
     */
    public void setACC(String aCC) {
        this.aCC = aCC;
    }

    /**
     * @return The cIB
     */
    public String getCIB() {
        return cIB;
    }

    /**
     * @param cIB The CIB
     */
    public void setCIB(String cIB) {
        this.cIB = cIB;
    }

    /**
     * @return The bILYET
     */
    public String getBILYET() {
        return bILYET;
    }

    /**
     * @param bILYET The BILYET
     */
    public void setBILYET(String bILYET) {
        this.bILYET = bILYET;
    }

    /**
     * @return The tGLBUKA
     */
    public String getTGLBUKA() {
        return tGLBUKA;
    }

    /**
     * @param tGLBUKA The TGLBUKA
     */
    public void setTGLBUKA(String tGLBUKA) {
        this.tGLBUKA = tGLBUKA;
    }

    /**
     * @return The tGLAKTIF
     */
    public String getTGLAKTIF() {
        return tGLAKTIF;
    }

    /**
     * @param tGLAKTIF The TGLAKTIF
     */
    public void setTGLAKTIF(String tGLAKTIF) {
        this.tGLAKTIF = tGLAKTIF;
    }

    /**
     * @return The tGLCAIR
     */
    public String getTGLCAIR() {
        return tGLCAIR;
    }

    /**
     * @param tGLCAIR The TGLCAIR
     */
    public void setTGLCAIR(String tGLCAIR) {
        this.tGLCAIR = tGLCAIR;
    }

    /**
     * @return The rEKST
     */
    public String getREKST() {
        return rEKST;
    }

    /**
     * @param rEKST The REKST
     */
    public void setREKST(String rEKST) {
        this.rEKST = rEKST;
    }

    /**
     * @return The nOSIMP
     */
    public String getNOSIMP() {
        return nOSIMP;
    }

    /**
     * @param nOSIMP The NOSIMP
     */
    public void setNOSIMP(String nOSIMP) {
        this.nOSIMP = nOSIMP;
    }

    /**
     * @return The jW
     */
    public String getJW() {
        return jW;
    }

    /**
     * @param jW The JW
     */
    public void setJW(String jW) {
        this.jW = jW;
    }

    /**
     * @return The pB
     */
    public String getPB() {
        return pB;
    }

    /**
     * @param pB The PB
     */
    public void setPB(String pB) {
        this.pB = pB;
    }

    /**
     * @return The rO
     */
    public String getRO() {
        return rO;
    }

    /**
     * @param rO The RO
     */
    public void setRO(String rO) {
        this.rO = rO;
    }

    /**
     * @return The bGA
     */
    public String getBGA() {
        return bGA;
    }

    /**
     * @param bGA The BGA
     */
    public void setBGA(String bGA) {
        this.bGA = bGA;
    }

    /**
     * @return The bUNGA
     */
    public String getBUNGA() {
        return bUNGA;
    }

    /**
     * @param bUNGA The BUNGA
     */
    public void setBUNGA(String bUNGA) {
        this.bUNGA = bUNGA;
    }

    /**
     * @return The pJK
     */
    public String getPJK() {
        return pJK;
    }

    /**
     * @param pJK The PJK
     */
    public void setPJK(String pJK) {
        this.pJK = pJK;
    }

    /**
     * @return The pAJAK
     */
    public String getPAJAK() {
        return pAJAK;
    }

    /**
     * @param pAJAK The PAJAK
     */
    public void setPAJAK(String pAJAK) {
        this.pAJAK = pAJAK;
    }

    /**
     * @return The pEN
     */
    public String getPEN() {
        return pEN;
    }

    /**
     * @param pEN The PEN
     */
    public void setPEN(String pEN) {
        this.pEN = pEN;
    }

    /**
     * @return The pENALTY
     */
    public String getPENALTY() {
        return pENALTY;
    }

    /**
     * @param pENALTY The PENALTY
     */
    public void setPENALTY(String pENALTY) {
        this.pENALTY = pENALTY;
    }

    /**
     * @return The nOMINAL
     */
    public String getNOMINAL() {
        return nOMINAL;
    }

    /**
     * @param nOMINAL The NOMINAL
     */
    public void setNOMINAL(String nOMINAL) {
        this.nOMINAL = nOMINAL;
    }

    /**
     * @return The sT
     */
    public String getST() {
        return sT;
    }

    /**
     * @param sT The ST
     */
    public void setST(String sT) {
        this.sT = sT;
    }

    /**
     * @return The tGLAPV
     */
    public String getTGLAPV() {
        return tGLAPV;
    }

    /**
     * @param tGLAPV The TGL_APV
     */
    public void setTGLAPV(String tGLAPV) {
        this.tGLAPV = tGLAPV;
    }

    /**
     * @return The uIDAPV
     */
    public String getUIDAPV() {
        return uIDAPV;
    }

    /**
     * @param uIDAPV The UID_APV
     */
    public void setUIDAPV(String uIDAPV) {
        this.uIDAPV = uIDAPV;
    }

    /**
     * @return The uID
     */
    public String getUID() {
        return uID;
    }

    /**
     * @param uID The UID
     */
    public void setUID(String uID) {
        this.uID = uID;
    }

    /**
     * @return The kU
     */
    public String getKU() {
        return kU;
    }

    /**
     * @param kU The KU
     */
    public void setKU(String kU) {
        this.kU = kU;
    }

    /**
     * @return The cIF
     */
    public String getCIF() {
        return cIF;
    }

    /**
     * @param cIF The CIF
     */
    public void setCIF(String cIF) {
        this.cIF = cIF;
    }

    /**
     * @return The aCCBB
     */
    public String getACCBB() {
        return aCCBB;
    }

    /**
     * @param aCCBB The ACCBB
     */
    public void setACCBB(String aCCBB) {
        this.aCCBB = aCCBB;
    }

    /**
     * @return The kETERANGAN
     */
    public String getKETERANGAN() {
        return kETERANGAN;
    }

    /**
     * @param kETERANGAN The KETERANGAN
     */
    public void setKETERANGAN(String kETERANGAN) {
        this.kETERANGAN = kETERANGAN;
    }

    /**
     * @return The gOLONGAN
     */
    public String getGOLONGAN() {
        return gOLONGAN;
    }

    /**
     * @param gOLONGAN The GOLONGAN
     */
    public void setGOLONGAN(String gOLONGAN) {
        this.gOLONGAN = gOLONGAN;
    }

    /**
     * @return The mutasi
     */
    public ArrayList<DetailDeposito> getMutasi() {
        return mutasi;
    }

    /**
     * @param mutasi The mutasi
     */
    public void setMutasi(ArrayList<DetailDeposito> mutasi) {
        this.mutasi = mutasi;
    }

}

