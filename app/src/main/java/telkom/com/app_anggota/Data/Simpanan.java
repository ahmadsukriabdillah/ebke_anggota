package telkom.com.app_anggota.Data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by sukri on 25/11/16.
 */

public class Simpanan implements Parcelable {
    public static final Creator<Simpanan> CREATOR = new Creator<Simpanan>() {
        @Override
        public Simpanan createFromParcel(Parcel source) {
            return new Simpanan(source);
        }

        @Override
        public Simpanan[] newArray(int size) {
            return new Simpanan[size];
        }
    };
    private int id;
    private String nosimp, acc, keterangan, saldo, tglbuka, tglaktif, tglbeban, debet, kredit;
    private ArrayList<ListSimpanan> simp;

    public Simpanan() {
    }

    public Simpanan(int id, String nosimp, String acc, String keterangan, ArrayList<ListSimpanan> simp) {
        this.id = id;
        this.nosimp = nosimp;
        this.acc = acc;
        this.keterangan = keterangan;
        this.simp = simp;
    }

    protected Simpanan(Parcel in) {
        this.id = in.readInt();
        this.nosimp = in.readString();
        this.acc = in.readString();
        this.keterangan = in.readString();
        this.saldo = in.readString();
        this.tglbuka = in.readString();
        this.tglaktif = in.readString();
        this.tglbeban = in.readString();
        this.simp = in.createTypedArrayList(ListSimpanan.CREATOR);
    }

    public String getTglbuka() {
        return tglbuka;
    }

    public void setTglbuka(String tglbuka) {
        this.tglbuka = tglbuka;
    }

    public String getDebet() {
        return debet;
    }

    public void setDebet(String debet) {
        this.debet = debet;
    }

    public String getKredit() {
        return kredit;
    }

    public void setKredit(String kredit) {
        this.kredit = kredit;
    }

    public String getTglaktif() {
        return tglaktif;
    }

    public void setTglaktif(String tglaktif) {
        this.tglaktif = tglaktif;
    }

    public String getTglbeban() {
        return tglbeban;
    }

    public void setTglbeban(String tglbeban) {
        this.tglbeban = tglbeban;
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNosimp() {
        return nosimp;
    }

    public void setNosimp(String nosimp) {
        this.nosimp = nosimp;
    }

    public String getAcc() {
        return acc;
    }

    public void setAcc(String acc) {
        this.acc = acc;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public ArrayList<ListSimpanan> getSimp() {
        return simp;
    }

    public void setSimp(ArrayList<ListSimpanan> simp) {
        this.simp = simp;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.nosimp);
        dest.writeString(this.acc);
        dest.writeString(this.keterangan);
        dest.writeString(this.saldo);
        dest.writeString(this.tglbuka);
        dest.writeString(this.tglaktif);
        dest.writeString(this.tglbeban);
        dest.writeTypedList(this.simp);
    }
}
