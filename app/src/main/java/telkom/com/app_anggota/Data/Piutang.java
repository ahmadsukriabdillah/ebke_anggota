package telkom.com.app_anggota.Data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by sukri on 01/12/16.
 */

public class Piutang implements Parcelable {
    public static final Creator<Piutang> CREATOR = new Creator<Piutang>() {
        @Override
        public Piutang createFromParcel(Parcel source) {
            return new Piutang(source);
        }

        @Override
        public Piutang[] newArray(int size) {
            return new Piutang[size];
        }
    };
    private String NOPINJ, IDLOS, ACC, KD_SBR, CIB, PB, GPA, BGA, PROV, ADMIN, CPP, JW, TGLAKAD, TGLBEBAN, TGLANGS,
            JATUHTEMPO, PLAFOND, OUTSTANDING, PENAMBAHAN, ANGSPOKOK, ANGSBUNGA, ANGSURAN, SBP, KE, NOMANGSPOKOK,
            NOMANGSBUNGA, NOMANGSURAN, NOMDENDDA, NOMPENALTY, NOMANGSPOKOK_CONVR, NOMANGSBUNGA_CONVR, NOMANGSURAN_CONVR,
            AG, AGR, RESUME, KOMENTAR, KETERANGAN;
    private ArrayList<DetailPiutang> mutasi;
    private String CALL;

    public Piutang() {
    }

    protected Piutang(Parcel in) {
        this.NOPINJ = in.readString();
        this.IDLOS = in.readString();
        this.ACC = in.readString();
        this.KD_SBR = in.readString();
        this.CIB = in.readString();
        this.PB = in.readString();
        this.GPA = in.readString();
        this.BGA = in.readString();
        this.PROV = in.readString();
        this.ADMIN = in.readString();
        this.CPP = in.readString();
        this.JW = in.readString();
        this.TGLAKAD = in.readString();
        this.TGLBEBAN = in.readString();
        this.TGLANGS = in.readString();
        this.JATUHTEMPO = in.readString();
        this.PLAFOND = in.readString();
        this.OUTSTANDING = in.readString();
        this.PENAMBAHAN = in.readString();
        this.ANGSPOKOK = in.readString();
        this.ANGSBUNGA = in.readString();
        this.ANGSURAN = in.readString();
        this.SBP = in.readString();
        this.KE = in.readString();
        this.NOMANGSPOKOK = in.readString();
        this.NOMANGSBUNGA = in.readString();
        this.NOMANGSURAN = in.readString();
        this.NOMDENDDA = in.readString();
        this.NOMPENALTY = in.readString();
        this.NOMANGSPOKOK_CONVR = in.readString();
        this.NOMANGSBUNGA_CONVR = in.readString();
        this.NOMANGSURAN_CONVR = in.readString();
        this.AG = in.readString();
        this.AGR = in.readString();
        this.RESUME = in.readString();
        this.KOMENTAR = in.readString();
        this.KETERANGAN = in.readString();
        this.mutasi = new ArrayList<DetailPiutang>();
        in.readList(this.mutasi, DetailPiutang.class.getClassLoader());
    }

    public String getKETERANGAN() {
        return KETERANGAN;
    }

    public void setKETERANGAN(String KETERANGAN) {
        this.KETERANGAN = KETERANGAN;
    }

    public String getNOPINJ() {
        return NOPINJ;
    }

    public void setNOPINJ(String NOPINJ) {
        this.NOPINJ = NOPINJ;
    }

    public String getIDLOS() {
        return IDLOS;
    }

    public void setIDLOS(String IDLOS) {
        this.IDLOS = IDLOS;
    }

    public String getACC() {
        return ACC;
    }

    public void setACC(String ACC) {
        this.ACC = ACC;
    }

    public String getKD_SBR() {
        return KD_SBR;
    }

    public void setKD_SBR(String KD_SBR) {
        this.KD_SBR = KD_SBR;
    }

    public String getCIB() {
        return CIB;
    }

    public void setCIB(String CIB) {
        this.CIB = CIB;
    }

    public String getPB() {
        return PB;
    }

    public void setPB(String PB) {
        this.PB = PB;
    }

    public String getGPA() {
        return GPA;
    }

    public void setGPA(String GPA) {
        this.GPA = GPA;
    }

    public String getBGA() {
        return BGA;
    }

    public void setBGA(String BGA) {
        this.BGA = BGA;
    }

    public String getPROV() {
        return PROV;
    }

    public void setPROV(String PROV) {
        this.PROV = PROV;
    }

    public String getADMIN() {
        return ADMIN;
    }

    public void setADMIN(String ADMIN) {
        this.ADMIN = ADMIN;
    }

    public String getCPP() {
        return CPP;
    }

    public void setCPP(String CPP) {
        this.CPP = CPP;
    }

    public String getJW() {
        return JW;
    }

    public void setJW(String JW) {
        this.JW = JW;
    }

    public String getTGLAKAD() {
        return TGLAKAD;
    }

    public void setTGLAKAD(String TGLAKAD) {
        this.TGLAKAD = TGLAKAD;
    }

    public String getTGLBEBAN() {
        return TGLBEBAN;
    }

    public void setTGLBEBAN(String TGLBEBAN) {
        this.TGLBEBAN = TGLBEBAN;
    }

    public String getTGLANGS() {
        return TGLANGS;
    }

    public void setTGLANGS(String TGLANGS) {
        this.TGLANGS = TGLANGS;
    }

    public String getJATUHTEMPO() {
        return JATUHTEMPO;
    }

    public void setJATUHTEMPO(String JATUHTEMPO) {
        this.JATUHTEMPO = JATUHTEMPO;
    }

    public String getPLAFOND() {
        return PLAFOND;
    }

    public void setPLAFOND(String PLAFOND) {
        this.PLAFOND = PLAFOND;
    }

    public String getOUTSTANDING() {
        return OUTSTANDING;
    }

    public void setOUTSTANDING(String OUTSTANDING) {
        this.OUTSTANDING = OUTSTANDING;
    }

    public String getPENAMBAHAN() {
        return PENAMBAHAN;
    }

    public void setPENAMBAHAN(String PENAMBAHAN) {
        this.PENAMBAHAN = PENAMBAHAN;
    }

    public String getANGSPOKOK() {
        return ANGSPOKOK;
    }

    public void setANGSPOKOK(String ANGSPOKOK) {
        this.ANGSPOKOK = ANGSPOKOK;
    }

    public String getANGSBUNGA() {
        return ANGSBUNGA;
    }

    public void setANGSBUNGA(String ANGSBUNGA) {
        this.ANGSBUNGA = ANGSBUNGA;
    }

    public String getANGSURAN() {
        return ANGSURAN;
    }

    public void setANGSURAN(String ANGSURAN) {
        this.ANGSURAN = ANGSURAN;
    }

    public String getSBP() {
        return SBP;
    }

    public void setSBP(String SBP) {
        this.SBP = SBP;
    }

    public String getKE() {
        return KE;
    }

    public void setKE(String KE) {
        this.KE = KE;
    }

    public String getNOMANGSPOKOK() {
        return NOMANGSPOKOK;
    }

    public void setNOMANGSPOKOK(String NOMANGSPOKOK) {
        this.NOMANGSPOKOK = NOMANGSPOKOK;
    }

    public String getNOMANGSBUNGA() {
        return NOMANGSBUNGA;
    }

    public void setNOMANGSBUNGA(String NOMANGSBUNGA) {
        this.NOMANGSBUNGA = NOMANGSBUNGA;
    }

    public String getNOMANGSURAN() {
        return NOMANGSURAN;
    }

    public void setNOMANGSURAN(String NOMANGSURAN) {
        this.NOMANGSURAN = NOMANGSURAN;
    }

    public String getNOMDENDDA() {
        return NOMDENDDA;
    }

    public void setNOMDENDDA(String NOMDENDDA) {
        this.NOMDENDDA = NOMDENDDA;
    }

    public String getNOMPENALTY() {
        return NOMPENALTY;
    }

    public void setNOMPENALTY(String NOMPENALTY) {
        this.NOMPENALTY = NOMPENALTY;
    }

    public String getNOMANGSPOKOK_CONVR() {
        return NOMANGSPOKOK_CONVR;
    }

    public void setNOMANGSPOKOK_CONVR(String NOMANGSPOKOK_CONVR) {
        this.NOMANGSPOKOK_CONVR = NOMANGSPOKOK_CONVR;
    }

    public String getNOMANGSBUNGA_CONVR() {
        return NOMANGSBUNGA_CONVR;
    }

    public void setNOMANGSBUNGA_CONVR(String NOMANGSBUNGA_CONVR) {
        this.NOMANGSBUNGA_CONVR = NOMANGSBUNGA_CONVR;
    }

    public String getNOMANGSURAN_CONVR() {
        return NOMANGSURAN_CONVR;
    }

    public void setNOMANGSURAN_CONVR(String NOMANGSURAN_CONVR) {
        this.NOMANGSURAN_CONVR = NOMANGSURAN_CONVR;
    }

    public String getAG() {
        return AG;
    }

    public void setAG(String AG) {
        this.AG = AG;
    }

    public String getAGR() {
        return AGR;
    }

    public void setAGR(String AGR) {
        this.AGR = AGR;
    }

    public String getRESUME() {
        return RESUME;
    }

    public void setRESUME(String RESUME) {
        this.RESUME = RESUME;
    }

    public String getKOMENTAR() {
        return KOMENTAR;
    }

    public void setKOMENTAR(String KOMENTAR) {
        this.KOMENTAR = KOMENTAR;
    }

    public ArrayList<DetailPiutang> getMutasi() {
        return mutasi;
    }

    public void setMutasi(ArrayList<DetailPiutang> mutasi) {
        this.mutasi = mutasi;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.NOPINJ);
        dest.writeString(this.IDLOS);
        dest.writeString(this.ACC);
        dest.writeString(this.KD_SBR);
        dest.writeString(this.CIB);
        dest.writeString(this.PB);
        dest.writeString(this.GPA);
        dest.writeString(this.BGA);
        dest.writeString(this.PROV);
        dest.writeString(this.ADMIN);
        dest.writeString(this.CPP);
        dest.writeString(this.JW);
        dest.writeString(this.TGLAKAD);
        dest.writeString(this.TGLBEBAN);
        dest.writeString(this.TGLANGS);
        dest.writeString(this.JATUHTEMPO);
        dest.writeString(this.PLAFOND);
        dest.writeString(this.OUTSTANDING);
        dest.writeString(this.PENAMBAHAN);
        dest.writeString(this.ANGSPOKOK);
        dest.writeString(this.ANGSBUNGA);
        dest.writeString(this.ANGSURAN);
        dest.writeString(this.SBP);
        dest.writeString(this.KE);
        dest.writeString(this.NOMANGSPOKOK);
        dest.writeString(this.NOMANGSBUNGA);
        dest.writeString(this.NOMANGSURAN);
        dest.writeString(this.NOMDENDDA);
        dest.writeString(this.NOMPENALTY);
        dest.writeString(this.NOMANGSPOKOK_CONVR);
        dest.writeString(this.NOMANGSBUNGA_CONVR);
        dest.writeString(this.NOMANGSURAN_CONVR);
        dest.writeString(this.AG);
        dest.writeString(this.AGR);
        dest.writeString(this.RESUME);
        dest.writeString(this.KOMENTAR);
        dest.writeString(this.KETERANGAN);
        dest.writeList(this.mutasi);
    }

    public String getCALL() {
        return CALL;
    }

    public void setCALL(String CALL) {
        this.CALL = CALL;
    }
}
