package telkom.com.app_anggota.Data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sukri on 25/11/16.
 */

public class ListSimpanan implements Parcelable {
    public static final Creator<ListSimpanan> CREATOR = new Creator<ListSimpanan>() {
        @Override
        public ListSimpanan createFromParcel(Parcel source) {
            return new ListSimpanan(source);
        }

        @Override
        public ListSimpanan[] newArray(int size) {
            return new ListSimpanan[size];
        }
    };
    private int id;
    private String tglStor, nosimp, keterangan, saldo, debit, kredit, berita;

    public ListSimpanan(int id, String tglStor, String nosimp, String keterangan, String saldo, String debit, String kredit) {
        this.id = id;
        this.tglStor = tglStor;
        this.nosimp = nosimp;
        this.keterangan = keterangan;
        this.saldo = saldo;
        this.debit = debit;
        this.kredit = kredit;
    }

    public ListSimpanan() {
    }

    protected ListSimpanan(Parcel in) {
        this.id = in.readInt();
        this.tglStor = in.readString();
        this.nosimp = in.readString();
        this.keterangan = in.readString();
        this.saldo = in.readString();
        this.debit = in.readString();
        this.kredit = in.readString();
    }

    public String getBerita() {
        return berita;
    }

    public void setBerita(String berita) {
        this.berita = berita;
    }

    public String getNosimp() {
        return nosimp;
    }

    public void setNosimp(String nosimp) {
        this.nosimp = nosimp;
    }

    public String getDebit() {
        return debit;
    }

    public void setDebit(String debit) {
        this.debit = debit;
    }

    public String getKredit() {
        return kredit;
    }

    public void setKredit(String kredit) {
        this.kredit = kredit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTglStor() {
        return tglStor;
    }

    public void setTglStor(String tglStor) {
        this.tglStor = tglStor;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.tglStor);
        dest.writeString(this.nosimp);
        dest.writeString(this.keterangan);
        dest.writeString(this.saldo);
        dest.writeString(this.debit);
        dest.writeString(this.kredit);
    }
}
