package telkom.com.app_anggota.Data;

/**
 * Created by sukri on 29/11/16.
 */
public class Profile {
    private int image;
    private String title, keterangan;
    private int viewType = 1;

    public Profile() {
    }

    public Profile(int image, String title, String keterangan, int viewType) {
        this.image = image;
        this.title = title;
        this.viewType = viewType;
        this.keterangan = keterangan;
    }

    public Profile(int image, String title, String keterangan) {
        this.image = image;
        this.title = title;
        this.keterangan = keterangan;
    }

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return title + ":";
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
}
