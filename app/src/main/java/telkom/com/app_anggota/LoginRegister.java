package telkom.com.app_anggota;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import telkom.com.app_anggota.Fragment.ImpLogin;
import telkom.com.app_anggota.Fragment.LoginFragment;
import telkom.com.app_anggota.Fragment.RegisterFragment;
import telkom.com.app_anggota.Fragment.Splash;
import telkom.com.app_anggota.Fragment.impRegister;
import telkom.com.app_anggota.Helper.AppConfig;
import telkom.com.app_anggota.Helper.AppController;
import telkom.com.app_anggota.Helper.CallDialog;
import telkom.com.app_anggota.Helper.SessionManager;

public class LoginRegister extends AppCompatActivity implements ImpLogin, impRegister, FragmentManager.OnBackStackChangedListener {
    private final static String TAG_LOGIN = "LOGIN";
    private final static String TAG_REGISTER = "REGISTER";
    private static int st = 0;
    private static String TAG_SPALSH = "SPLASH";
    private Toolbar toolbar;
    private String state;
    private ProgressDialog pDialog;
    private SessionManager session;

    private FragmentTransaction ft;
    private Fragment fragment;
    private boolean doubleBackToExitPressedOnce = false;
    private int tray = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_register);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("MCB - Personal Assistant");
        toolbar.setVisibility(View.GONE);
        setSupportActionBar(toolbar);
        pDialog = new ProgressDialog(this);
        session = new SessionManager(this);
        ft = getSupportFragmentManager().beginTransaction();
        if (savedInstanceState == null) {
            fragment = new Splash().newInstance();
            ft.add(R.id.content_login_register, fragment, TAG_SPALSH);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            ft.commit();
            Handler a = new Handler();
            a.postDelayed(new Runnable() {
                @Override
                public void run() {
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    fragment = new LoginFragment().newInstance();
                    ft.replace(R.id.content_login_register, fragment, TAG_LOGIN);
                    ft.commit();
                    state = TAG_LOGIN;
                    toolbar.setVisibility(View.VISIBLE);
                    setTitle("MCB - Personal Assistant");

                }
            }, 2000);
        } else {
            toolbar.setVisibility(View.VISIBLE);
            return;
//            try {
//                toolbar.setVisibility(View.VISIBLE);
//                state = savedInstanceState.getString("state");
//                if(getSupportFragmentManager().findFragmentByTag(savedInstanceState.getString("state")) == null){
//                    switch (savedInstanceState.getString("state")){
//                        case TAG_LOGIN:
//                            fragment = new RegisterFragment().newInstance();;
//                            break;
//                        case TAG_REGISTER:
//                            fragment = new RegisterFragment().newInstance();
//                            break;
//                    }
//                    ft.replace(R.id.content_login_register, fragment,TAG_REGISTER);
//                    ft.commit();
//                }else{
//                    fragment = getSupportFragmentManager().findFragmentByTag(savedInstanceState.getString("state"));
//                    getSupportFragmentManager().beginTransaction()
//                            .show(fragment).commit();
//                }
//
//            }catch (Exception e){
//                Log.d("eroor",e.getMessage());
//
//            }


//            if(st == 0){
//                ft.add(R.id.content_login_register, new Splash().newInstance(),TAG_SPALSH);
//                ft.commit();
//                android.os.Handler a = new android.os.Handler();
//                a.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//                        ft.replace(R.id.content_login_register, new LoginFragment().newInstance(),TAG_LOGIN);
//                        ft.commit();
//                        toolbar.setVisibility(View.VISIBLE);
//                        setTitle("MCB - Personal Assistant");
//                        st = 1;
//                    }
//                },2000);
//
//            }else if(st == 2){
//                ft.add(R.id.content_login_register, new RegisterFragment().newInstance()).addToBackStack("a"),TAG_REGISTER);
//                ft.commit();
//                toolbar.setVisibility(View.VISIBLE);
//                setTitle("MCB - Personal Assistant");
//            }else{
//                ft.replace(R.id.content_login_register, new LoginFragment().newInstance(),TAG_LOGIN);
//                ft.commit();
//                toolbar.setVisibility(View.VISIBLE);
//                setTitle("MCB - Personal Assistant");
//            }
        }


        getSupportFragmentManager().addOnBackStackChangedListener(this);
    }

    @Override
    public void onBackStackChanged() {
        shouldDisplayHomeUp();
    }

    public void shouldDisplayHomeUp() {
        //Enable Up button only  if there are entries in the back stack
        boolean canback = getSupportFragmentManager().getBackStackEntryCount() > 0;
        getSupportActionBar().setDisplayHomeAsUpEnabled(canback);
    }

    @Override
    public boolean onSupportNavigateUp() {
        getSupportFragmentManager().popBackStack();
        return true;
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        state = savedInstanceState.getString("state");
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("state", state);
    }

    private void navigate(Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        this.fragment = fragment;
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_login_register, fragment, TAG_REGISTER).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .addToBackStack("a").commit();
        state = TAG_REGISTER;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ft.commit();

    }

    private void setTitle(String title) {
        toolbar.setTitle(title);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
            return;
        }

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    public void login(final String username, final String password) {
        String tag_string_req = "req_login";

        pDialog.setMessage("Loading..");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.LOGIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                //Log.d("login", "Login Response: " + response.toString());
                hideDialog();
                String errorMsg = null;

                try {

                    JSONObject object = new JSONObject(response);
                    String error = object.getString("status");
                    if (error.equalsIgnoreCase("true")) {
                        JSONArray a = object.getJSONArray("data");
                        JSONObject job = a.getJSONObject(0);
                        session.setID(job.getString("UID"), job.getString("CIF"), job.getString("CIB"));

                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        finish();

                    } else {
                        ((LoginFragment) fragment).setErrorLogin();
                        Toast.makeText(getApplicationContext(), "Username / Password Salah", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e("login", "Login Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(),
                //        error.getMessage(), Toast.LENGTH_LONG).show();
                //error.networkResponse.statusCode
                hideDialog();
                if (tray < 5) {
                    tray++;
                    CallDialog.showMassage(LoginRegister.this, "Erorr", "Terjadi Kesalahan Coba Lagi");
                } else {
                    CallDialog.showMassage(LoginRegister.this, "Erorr", "Server Bermasalah coba Beberapa saat kemudian");
                }

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);
                params.put("password", password);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public void toRegister() {
        navigate(new RegisterFragment().newInstance());
    }

    @Override
    public void register(final HashMap<String, String> data) {
        String tag_string_req = "req_daftar";

        pDialog.setMessage("Loading..");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.DAFTAR, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("login", "Login Response: " + response.toString());
                hideDialog();
                String errorMsg = null;

                try {

                    JSONObject object = new JSONObject(response);
                    boolean error = object.getBoolean("status");
                    if (!error) {

//                            String id = object.getString("id");
//                            String cif = object.getString("cif");
//                            String nama = object.getString("nama");
//
//                            session.setID(nama,cif,id);
//                            session.setLogin(true);
//                        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
//                        startActivity(intent);
//                        finish();
                        showAlertSucces();


                    } else {
                        Toast.makeText(getApplicationContext(), "Pendaftaran Gagal", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("login", "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = data;
                return params;
            }

        };

        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public void toLogin() {

    }

    @Override
    public void openDialog(int id, String title) {

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void showAlertSucces() {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginRegister.this);

        builder.setMessage("Silahkan hubungu mitra koperasi untuk mengaktifkan fitur mobile koperasi");
        builder.setTitle("Informasi");
        builder.setPositiveButton("Terima Kasih", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                getSupportFragmentManager().popBackStack();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
