package telkom.com.app_anggota.Helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class SessionManager {
    // Shared preferences file name
    private static final String PREF_NAME = "m-asistent";
    private static final String KEY_IS_LOGIN = "isLoggedInAdmin";
    private static final String KEY_USERNAME = "nama";
    private static final String KEY_CIF = "cif";
    private static final String KEY_CIB = "id";
    // LogCat tag
    private static String TAG = SessionManager.class.getSimpleName();
    // Shared Preferences
    SharedPreferences pref;
    Editor editor;
    Context _context;
    // Shared pref mode
    int PRIVATE_MODE = 0;


    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setLogin(boolean admin) {

        editor.putBoolean(KEY_IS_LOGIN, admin);
        editor.commit();
        Log.d(TAG, "User login session modified!");
    }

    public void setID(String username, String CIF, String CIB) {

        editor.putString(KEY_USERNAME, username);
        editor.putString(KEY_CIF, CIF);

        editor.putString(KEY_CIB, CIB);
        editor.commit();

        Log.d(TAG, "User ID session modified!");
    }


    public boolean isLoggedIn() {
        return pref.getBoolean(KEY_IS_LOGIN, false);
    }

    public String getCIF() {
        return pref.getString(KEY_CIF, "");
    }

    public String getUSERNAME() {
        return pref.getString(KEY_USERNAME, "");
    }

    public String getCIB() {
        return pref.getString(KEY_CIB, "");
    }

}
