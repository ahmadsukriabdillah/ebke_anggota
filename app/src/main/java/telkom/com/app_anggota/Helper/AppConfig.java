package telkom.com.app_anggota.Helper;

/**
 * Created by sukri on 22/10/16.
 */

public class AppConfig {

    public static final String SERVER = "http://e-kopbke.com/AndroidAnggota/";
    public static final String LOGIN = SERVER + "login.php";
    public static final String DAFTAR = SERVER + "register.php";
    public static final String LIST_DAFTAR = SERVER + "list.php";
    public static final String SLIDE = SERVER + "slide.php";
    public static final String FETCH_SIMP = SERVER + "simpanan.php";
    public static final String FETCH_PIUT = SERVER + "piutang.php";
    public static final String FETCH_DEP = SERVER + "deposito.php";
    public static final String PROFILE = SERVER + "profile.php";
    public static final String DETAIL_PIUTANG = SERVER + "d_piutang.php";
    public static final String CHPASS = SERVER + "chpass.php";
}
