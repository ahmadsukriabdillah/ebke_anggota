package telkom.com.app_anggota.Helper;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Created by sukri on 06/12/16.
 */

public class CallDialog {

    public static void showDialogConfirm(Context con,String title,String massage,Object a){
        AlertDialog.Builder builder = new AlertDialog.Builder(con);
        builder.setMessage("do you wish to sign out?");
        builder.setTitle("Sign Out");

        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });

        builder.setNegativeButton("Cancel", new   DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int id){

            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    public static void showMassage(Context con,String title,String massage){
        AlertDialog.Builder builder = new AlertDialog.Builder(con);
        builder.setMessage(massage);
        builder.setTitle(title);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
